
#include "namespaces.h"

#include "raylib.h"
#include "rlgl.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

static int nsk__align;
static unsigned nsk__creso;

#define NSK_MAXPOINTS 1024

static Vector2 nsk__points[NSK_MAXPOINTS];
static unsigned nsk__points_size = 0;

static Color nsk__color;

static Font nsk__font;
static char nsk__font_path[1024]; // max 1024 characters
static int nsk__font_size = -1;
static int nsk__font_assigned = 0;

void nsk_canvas_set_defaults(void)
{
	nsk__color.r = 255;
	nsk__color.g = 255;
	nsk__color.b = 255;
	nsk__color.a = 255;
	nsk__align = 0;
	nsk__creso = 60;
	BeginBlendMode(BLEND_ALPHA);

	// this should go in window, but it's here to avoid too many default funcs
	SetTargetFPS(60);
}

void nsk_canvas_release(void)
{
	if (nsk__font_assigned) {
		UnloadFont(nsk__font);
		nsk__font_assigned = 0;
	}
}

// ------------------------------------------------------------------------------

static int nsk_color(lua_State * L)
{
	unsigned r = (unsigned)luaL_checkinteger(L, 1);
	unsigned g = (unsigned)luaL_checkinteger(L, 2);
	unsigned b = (unsigned)luaL_checkinteger(L, 3);
	unsigned a = (unsigned)luaL_checkinteger(L, 4);

	unsigned char cr = (unsigned char)(r & 0xff);
	unsigned char cg = (unsigned char)(g & 0xff);
	unsigned char cb = (unsigned char)(b & 0xff);
	unsigned char ca = (unsigned char)(a & 0xff);

	nsk__color.r = cr;
	nsk__color.g = cg;
	nsk__color.b = cb;
	nsk__color.a = ca;

	return 0;
}

static int nsk_font(lua_State * L)
{
	if (lua_gettop(L) >= 2) {
		const char * path = luaL_checkstring(L, 1);
		int size = (int)luaL_checkinteger(L, 2);
		if (strcmp(path, nsk__font_path) != 0 || size != nsk__font_size) {
			if (nsk__font_assigned) {
				UnloadFont(nsk__font);
			}
			memmove(nsk__font_path, path, strlen(path) + 1);
			nsk__font_size = size;
			nsk__font = LoadFontEx(path, size, 0, 0);
			nsk__font_assigned = 1;
		}
	} else {
		nsk__font = GetFontDefault();
		nsk__font_assigned = 1;
	}
	return 0;
}

static int nsk_text(lua_State * L)
{
	const char * msg = luaL_checkstring(L, 1);
	float x = (float)luaL_checknumber(L, 2);
	float y = (float)luaL_checknumber(L, 3);
	if (nsk__font_assigned) {
		DrawTextEx(nsk__font, msg, (Vector2){x, y}, (float)nsk__font.baseSize, 2, nsk__color);
	} else {
		DrawText(msg, (int)x, (int)y, 20, nsk__color);
	}
	return 0;
}

static int nsk_point(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);

	DrawLine(x, y, x, y + 1, nsk__color);
	return 0;
}

static int nsk_line(lua_State * L)
{
	int x0 = (int)luaL_checknumber(L, 1);
	int y0 = (int)luaL_checknumber(L, 2);
	int x1 = (int)luaL_checknumber(L, 3);
	int y1 = (int)luaL_checknumber(L, 4);

	DrawLine(x0, y0, x1, y1, nsk__color);
	return 0;
}

static int nsk_thick_line(lua_State * L)
{
	Vector2 v0, v1;
	v0.x = (float)luaL_checknumber(L, 1);
	v0.y = (float)luaL_checknumber(L, 2);
	v1.x = (float)luaL_checknumber(L, 3);
	v1.y = (float)luaL_checknumber(L, 4);
	float t = (float)luaL_checknumber(L, 5);
	DrawLineEx(v0, v1, t, nsk__color);
	return 0;
}

static int nsk_bezier(lua_State * L)
{
	Vector2 v0, v1;
	Vector2 c;
	v0.x = (float)luaL_checknumber(L, 1);
	v0.y = (float)luaL_checknumber(L, 2);
	v1.x = (float)luaL_checknumber(L, 3);
	v1.y = (float)luaL_checknumber(L, 4);
	c.x = (float)luaL_checknumber(L, 5);
	c.y = (float)luaL_checknumber(L, 6);
	float t = (float)luaL_optnumber(L, 7, 1.0f);
	DrawLineBezierQuad(v0, v1, c, t, nsk__color);
	return 0;
}

static int nsk_vertex(lua_State * L)
{
	nsk__points[nsk__points_size].x = (float)luaL_checknumber(L, 1);
	nsk__points[nsk__points_size].y = (float)luaL_checknumber(L, 2);
	nsk__points_size++;
	if (nsk__points_size == NSK_MAXPOINTS) {
		printf("[nsk] can't add more points to shape or line, max reached\n");
		nsk__points_size--;
	}
	return 0;
}

static int nsk_begin_line(lua_State * L)
{
	nsk__points_size = 0;
	return 0;
	(void)L;
}

static int nsk_end_line(lua_State * L)
{
	DrawLineStrip(nsk__points, (int)nsk__points_size, nsk__color);
	return 0;
	(void)L;
}

static int nsk_close_line(lua_State * L)
{
	if (nsk__points_size < NSK_MAXPOINTS - 1) {
		nsk__points[nsk__points_size].x = nsk__points[0].x;
		nsk__points[nsk__points_size].y = nsk__points[0].y;
		nsk__points_size++;
	}
	DrawLineStrip(nsk__points, (int)nsk__points_size, nsk__color);
	return 0;
	(void)L;
}

static int nsk_begin_shape(lua_State * L)
{
	nsk__points_size = 0;
	return 0;
	(void)L;
}

static int nsk_close_shape(lua_State * L)
{
	if (nsk__points_size < NSK_MAXPOINTS - 1) {
		nsk__points[nsk__points_size].x = nsk__points[0].x;
		nsk__points[nsk__points_size].y = nsk__points[0].y;
		nsk__points_size++;
	}
	DrawTriangleStrip(nsk__points, (int)nsk__points_size, nsk__color);
	return 0;
	(void)L;
}

static int nsk_end_shape(lua_State * L)
{
	DrawTriangleStrip(nsk__points, (int)nsk__points_size, nsk__color);
	return 0;
	(void)L;
}

static int nsk_translate(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	rlTranslatef(x, y, 0.0f);
	return 0;
}

static int nsk_rotate(lua_State * L)
{
	float deg = (float)luaL_checknumber(L, 1);
	rlRotatef(deg * RAD2DEG, 0.0f, 0.0f, 1.0f);
	return 0;
}

static int nsk_scale(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y;
	int opt = lua_gettop(L);
	switch (opt) {
	case 1:
		y = x;
		rlScalef(x, y, 1.0f);
		return 0;
		break;
	case 2:
		y = (float)luaL_checknumber(L, 2);
		rlScalef(x, y, 1.0f);
		return 0;
		break;
	default:
		break;
	}
	printf("[nsk] no arguments for scale\n");
	return 0;
}

static int nsk_push_matrix(lua_State * L)
{
	rlPushMatrix();
	return 0;
	(void)L;
}

static int nsk_pop_matrix(lua_State * L)
{
	rlPopMatrix();
	return 0;
	(void)L;
}

static int nsk_align_corner(lua_State * L)
{
	nsk__align = 0;
	return 0;
	(void)L;
}

static int nsk_align_center(lua_State * L)
{
	nsk__align = 1;
	return 0;
	(void)L;
}

static int nsk_align_bottom(lua_State * L)
{
	nsk__align = 2;
	return 0;
	(void)L;
}

static inline void _do_rect(int x, int y, int w, int h)
{
	switch (nsk__align) {
	case 1:
		x -= w / 2;
		y -= h / 2;
		break;

	case 2:
		y -= h;
		break;

	default:
		break;
	}
	int x1 = x + w;
	int y1 = y + h;

	rlColor4ub(nsk__color.r, nsk__color.g, nsk__color.b, nsk__color.a);
	rlBegin(RL_LINES);
	rlVertex2i(x, y);
	rlVertex2i(x1, y);

	rlVertex2i(x1, y);
	rlVertex2i(x1, y1);

	rlVertex2i(x1, y1);
	rlVertex2i(x, y1);

	rlVertex2i(x, y1);
	rlVertex2i(x, y);
	rlEnd();
}

static int nsk_rect(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);
	int w = (int)luaL_checknumber(L, 3);
	int h = (int)luaL_checknumber(L, 4);
	_do_rect(x, y, w, h);
	return 0;
}

static int nsk_rect_fill(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);
	int w = (int)luaL_checknumber(L, 3);
	int h = (int)luaL_checknumber(L, 4);

	switch (nsk__align) {
	case 1:
		x -= w / 2;
		y -= h / 2;
		break;
	case 2:
		y -= h;
		break;
	default:
		break;
	}

	DrawRectangle(x, y, w, h, nsk__color);

	return 0;
}

static int nsk_square(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);
	int s = (int)luaL_checknumber(L, 3);
	_do_rect(x, y, s, s);
	return 0;
}

static int nsk_square_fill(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);
	int s = (int)luaL_checknumber(L, 3);
	switch (nsk__align) {
	case 1:
		x -= s / 2;
		y -= s / 2;
		break;
	case 2:
		y -= s;
		break;
	default:
		break;
	}
	DrawRectangle(x, y, s, s, nsk__color);
	return 0;
}

static int nsk_poly(lua_State * L)
{
	Vector2 c;
	c.x = (float)luaL_checknumber(L, 1);
	c.y = (float)luaL_checknumber(L, 2);
	float r = (float)luaL_checknumber(L, 3);
	int n = (int)luaL_checkinteger(L, 4);
	float theta = (float)luaL_optnumber(L, 5, 0.0);

	DrawPolyLines(c, n, r, theta * RAD2DEG, nsk__color);
	return 0;
}

static int nsk_poly_fill(lua_State * L)
{
	Vector2 c;
	c.x = (float)luaL_checknumber(L, 1);
	c.y = (float)luaL_checknumber(L, 2);
	float r = (float)luaL_checknumber(L, 3);
	int n = (int)luaL_checkinteger(L, 4);
	float theta = (float)luaL_optnumber(L, 5, 0.0);

	DrawPoly(c, n, r, theta * RAD2DEG, nsk__color);
	return 0;
}

static int nsk_circle(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);
	float r = (float)luaL_checknumber(L, 3);
	DrawCircleLines(x, y, r, nsk__color);
	return 0;
}

static int nsk_circle_fill(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);
	float r = (float)luaL_checknumber(L, 3);
	DrawCircle(x, y, r, nsk__color);
	return 0;
}

static int nsk_triangle(lua_State * L)
{
	Vector2 v0, v1, v2;
	v0.x = (float)luaL_checknumber(L, 1);
	v0.y = (float)luaL_checknumber(L, 2);
	v1.x = (float)luaL_checknumber(L, 3);
	v1.y = (float)luaL_checknumber(L, 4);
	v2.x = (float)luaL_checknumber(L, 5);
	v2.y = (float)luaL_checknumber(L, 6);
	DrawTriangleLines(v2, v1, v0, nsk__color);
	return 0;
}

static int nsk_triangle_fill(lua_State * L)
{
	Vector2 v0, v1, v2;
	v0.x = (float)luaL_checknumber(L, 1);
	v0.y = (float)luaL_checknumber(L, 2);
	v1.x = (float)luaL_checknumber(L, 3);
	v1.y = (float)luaL_checknumber(L, 4);
	v2.x = (float)luaL_checknumber(L, 5);
	v2.y = (float)luaL_checknumber(L, 6);
	DrawTriangle(v2, v1, v0, nsk__color);
	return 0;
}

static int nsk_blend_alpha(lua_State * L)
{
	BeginBlendMode(BLEND_ALPHA);
	return 0;
	(void)L;
}

static int nsk_blend_additive(lua_State * L)
{
	BeginBlendMode(BLEND_ADDITIVE);
	return 0;
	(void)L;
}

static int nsk_blend_multiply(lua_State * L)
{
	BeginBlendMode(BLEND_MULTIPLIED);
	return 0;
	(void)L;
}

static int nsk_blend_add_colors(lua_State * L)
{
	BeginBlendMode(BLEND_ADD_COLORS);
	return 0;
	(void)L;
}

static int nsk_blend_subtract_colors(lua_State * L)
{
	BeginBlendMode(BLEND_SUBTRACT_COLORS);
	return 0;
	(void)L;
}

// ------------------------------------------------------------------------------

static const luaL_Reg nsk_namespace[] = {
    {"color", nsk_color},
    {"point", nsk_point},
    {"line", nsk_line},
    {"thick_line", nsk_thick_line},
    {"bezier", nsk_bezier},
    {"rect", nsk_rect},
    {"rect_fill", nsk_rect_fill},
    {"square", nsk_square},
    {"square_fill", nsk_square_fill},
    {"poly", nsk_poly},
    {"poly_fill", nsk_poly_fill},
    {"circle", nsk_circle},
    {"circle_fill", nsk_circle_fill},
    {"triangle", nsk_triangle},
    {"triangle_fill", nsk_triangle_fill},
    {"align_corner", nsk_align_corner},
    {"align_center", nsk_align_center},
    {"align_bottom", nsk_align_bottom},
    {"vertex", nsk_vertex},
    {"begin_line", nsk_begin_line},
    {"end_line", nsk_end_line},
    {"close_line", nsk_close_line},
    {"begin_shape", nsk_begin_shape},
    {"close_shape", nsk_close_shape},
    {"end_shape", nsk_end_shape},
    {"translate", nsk_translate},
    {"rotate", nsk_rotate},
    {"push", nsk_push_matrix},
    {"pop", nsk_pop_matrix},
    {"scale", nsk_scale},
    {"blend_alpha", nsk_blend_alpha},
    {"blend_additive", nsk_blend_additive},
    {"blend_multiply", nsk_blend_multiply},
    {"blend_add_colors", nsk_blend_add_colors},
    {"blend_subtract_colors", nsk_blend_subtract_colors},
    {"font", nsk_font},
    {"text", nsk_text},
    {NULL, NULL}};

LUALIB_API int luaopen_nsk(lua_State * L)
{
	luaL_register(L, "gfx", nsk_namespace);
	return 1;
}
