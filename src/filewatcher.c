
#include "filewatcher.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

filewatcher_t * filewatcher_create(const char * file)
{
	filewatcher_t * watcher = malloc(sizeof(filewatcher_t));
	if (watcher == NULL) {
		printf("memory error during filewatcher allocation for file %s", file);
		return NULL;
	}

	watcher->ifd = inotify_init();
	watcher->wd = inotify_add_watch(watcher->ifd, file, IN_MODIFY);
	fcntl(watcher->ifd, F_SETFL, fcntl(watcher->ifd, F_GETFL) | O_NONBLOCK);

	return watcher;
}

void filewatcher_destroy(filewatcher_t * watcher)
{
	if (watcher != NULL) {
		inotify_rm_watch(watcher->ifd, watcher->wd);
		close(watcher->ifd);
		free(watcher);
	}
}

int filewatcher_change_detected(filewatcher_t * watcher)
{
	ssize_t ev_len = read(watcher->ifd, watcher->evb, EVENT_BUF_LEN);
	if (ev_len > 0) {
		return 1;
	}
	return 0;
}
