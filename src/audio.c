
#include "namespaces.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <jack/jack.h>

#include <math.h>
#ifndef M_TWO_PI
#define M_TWO_PI 6.2831853071795864769252867665590
#endif

#if __STDC_VERSION__ >= 201112L && !defined(__STDC_NO_ATOMICS__)
#include <stdatomic.h>
typedef _Atomic float atomic_float_t;
typedef _Atomic int atomic_int_t;
#else
#pragma message "you need a c11 compiler with atomics support!"
#endif

static jack_client_t * nsk__jack_client = NULL;
static int nsk__jack_is_open = 0;
static int nsk__jack_is_connected = 0;
static double nsk__samplerate = 0.0f;
static unsigned nsk__buffersize = 0;

#define NSK_JACK_DISCONNECTED 0
#define NSK_JACK_CONNECTED 1

typedef struct nsk_filter_data {
	float z1_1;
	float z1_2;
} nsk_filter_data;

typedef struct nsk_filter_control {
	atomic_float_t a0;
	atomic_float_t b1;
} nsk_filter_control;

// digital curve
// log( 0.01 ) = -4.605170
// in c the function log(value) is the natural logarhytm (ln)
// log(0.368)is more analog
#define ENV_TC -4.605170f

typedef struct nsk_env_control {
	atomic_float_t attack_coeff;
	atomic_float_t release_coeff;
	float tc;
} nsk_env_control;

typedef struct nsk_band {
	atomic_int_t use_hpf;
	nsk_filter_control hpfc;
	nsk_filter_data hpf;

	atomic_int_t use_lpf;
	nsk_filter_control lpfc;
	nsk_filter_data lpf;

	nsk_env_control env;
	float env_output;
	atomic_float_t peak;

	atomic_int_t active;
	jack_port_t * port;
	float * buffer;
} nsk_band;

#define NSK_MAX_BANDS 10
static nsk_band nsk_bands[NSK_MAX_BANDS];
unsigned int nsk__last_selected_band = 0;
unsigned int nsk__assigned_bands = 0;

// ------------------------------------------------------------------------------

int nsk__jack_process(jack_nframes_t nframes, void * arg)
{
	for (int b = 0; b < NSK_MAX_BANDS; ++b) {
		if (nsk_bands[b].active) {
			jack_default_audio_sample_t * in;
			in = (jack_default_audio_sample_t *)jack_port_get_buffer(nsk_bands[b].port, nframes);

			float * buffer = nsk_bands[b].buffer;

			for (jack_nframes_t n = 0; n < nframes; ++n) {
				buffer[n] = in[n];
			}

			if (nsk_bands[b].use_lpf) {
				nsk_filter_control lpfc = nsk_bands[b].lpfc;
				nsk_filter_data lpf = nsk_bands[b].lpf;

				for (jack_nframes_t n = 0; n < nframes; ++n) {
					float xn = buffer[n];

					// lpf first pole
					float yn = xn * lpfc.a0 + lpf.z1_1 * lpfc.b1;
					lpf.z1_1 = yn;
					xn = yn;

					// lpf second pole
					yn = xn * lpfc.a0 + lpf.z1_2 * lpfc.b1;
					lpf.z1_2 = yn;
					buffer[n] = yn;
				}

				nsk_bands[b].lpf = lpf;
			}

			if (nsk_bands[b].use_hpf) {
				nsk_filter_control hpfc = nsk_bands[b].hpfc;
				nsk_filter_data hpf = nsk_bands[b].hpf;

				for (jack_nframes_t n = 0; n < nframes; ++n) {
					float xn = buffer[n];

					// hpf first pole
					float yn = xn * hpfc.a0 + hpf.z1_1 * hpfc.b1;
					hpf.z1_1 = yn;
					xn = xn - yn;

					// hpf second pole
					yn = xn * hpfc.a0 + hpf.z1_2 * hpfc.b1;
					hpf.z1_2 = yn;
					buffer[n] = xn - yn;
				}

				nsk_bands[b].hpf = hpf;
			}

			nsk_env_control env = nsk_bands[b].env;
			float output = nsk_bands[b].env_output;
			float peak = -42000.0f;

			for (jack_nframes_t n = 0; n < nframes; ++n) {
				float xn = buffer[n];

				if (xn < 0.0f) { // absolute value
					xn = -xn;
				}

				float yn;

				if (xn > output) {
					yn = env.attack_coeff * (output - xn) + xn;
				} else {
					yn = env.release_coeff * (output - xn) + xn;
				}
				output = yn;

				if (yn > peak) {
					peak = yn;
				}
			}
			nsk_bands[b].env_output = output;
			nsk_bands[b].peak = peak;
		}
	}
	return 0;
	(void)arg;
}

static void nsk__jack_shutdown(void * arg)
{
	(void)arg;
	printf("jack closed! quitting nsketch...\n");
	exit(1);
}

void nsk_jack_init(void)
{
	for (int i = 0; i < NSK_MAX_BANDS; ++i) {
		nsk_bands[i].use_hpf = 0;
		nsk_bands[i].use_lpf = 0;
		nsk_bands[i].env_output = 0.0f;
		nsk_bands[i].active = 0;
		nsk_bands[i].port = NULL;
	}
}

void nsk_jack_close(void)
{
	if (nsk__jack_is_open) {
		jack_client_close(nsk__jack_client);
	}
	for (int i = 0; i < NSK_MAX_BANDS; ++i) {
		if (nsk_bands[i].active && nsk_bands[i].buffer != NULL) {
			free(nsk_bands[i].buffer);
		}
	}
}

void nsk_jack_logging(const char * message)
{
	printf("[audio] %s\n", message);
}

static int nsk__jack_open(void)
{
	if (nsk__jack_is_open == 0) {
		jack_set_error_function(nsk_jack_logging);
		jack_set_info_function(nsk_jack_logging);

		const char * client_name = "nsketch"; // TODO add PID for unique name?
		const char * server_name = NULL;
		jack_options_t options = JackNoStartServer;
		jack_status_t status;

		nsk__jack_client = jack_client_open(client_name, options,
		                                    &status, server_name);
		if (nsk__jack_client == NULL) {
			fprintf(stderr,
			        "[audio] jack_client_open() failed, "
			        "status = 0x%2.0x\n",
			        status);
			return 0;
		}
		if (status & JackServerStarted) {
			printf("[audio] JACK server started\n");
		}
		if (status & JackNameNotUnique) {
			client_name = jack_get_client_name(nsk__jack_client);
			printf("[audio] unique name `%s' assigned\n", client_name);
		}

		// ---- init resources
		nsk__samplerate = (double)jack_get_sample_rate(nsk__jack_client);
		nsk__buffersize = (unsigned)jack_get_buffer_size(nsk__jack_client);

		jack_set_process_callback(nsk__jack_client, nsk__jack_process, NULL);

		jack_on_shutdown(nsk__jack_client, nsk__jack_shutdown, 0);

		if (jack_activate(nsk__jack_client)) {
			fprintf(stderr, "[audio] cannot activate client");
			return -1;
		}

		nsk__jack_is_open = 1;
	}
	return 0;
}

// ------------------------------------------------------------------------------

/*
	MEMO: when a band is connected the script has to be reloaded 
			to change connection 
*/

static int nsk_band_id(lua_State * L)
{
	int rc = nsk__jack_open();
	if (rc != 0) {
		fprintf(stderr, "[audio] error opening JACK\n");
		return 0;
	}

	int bandi = (int)luaL_checkinteger(L, 1);
	unsigned id = (unsigned)luaL_checkinteger(L, 2);

	if (bandi < 0 || bandi > NSK_MAX_BANDS) {
		printf("[audio] band id %d is exceeding max (%d)\n", bandi, NSK_MAX_BANDS);
		return 0;
	}

	if (!nsk_bands[bandi].active &&
	    nsk__jack_is_open &&
	    //nsk__jack_is_connected == NSK_JACK_DISCONNECTED &&
	    nsk_bands[bandi].port == NULL) {

		char inputname[] = "nsketch_x";
		inputname[8] = (char)(nsk__assigned_bands + (unsigned)'0');
		nsk__assigned_bands++;

		nsk_bands[bandi].port = jack_port_register(nsk__jack_client,
		                                           inputname,
		                                           JACK_DEFAULT_AUDIO_TYPE,
		                                           JackPortIsInput, 0);

		if (nsk_bands[bandi].port == NULL) {
			fprintf(stderr, "[audio] no more JACK ports available\n");
			return -1;
		}

		const char ** ports;
		ports = jack_get_ports(nsk__jack_client, NULL, NULL, JackPortIsOutput);

		if (ports == NULL) {
			fprintf(stderr,
			        "[audio] no ports found for connecing band %d\n",
			        bandi);
			jack_free(ports);
			return 0;
		}

		if (jack_connect(nsk__jack_client, ports[id],
		                 jack_port_name(nsk_bands[bandi].port))) {
			fprintf(stderr, "[audio] cannot connect band %d input ports %d\n",
			        bandi, id);
			jack_free(ports);
			return 0;
		}

		nsk__jack_is_connected = NSK_JACK_CONNECTED;
		jack_free(ports);
		printf("[audio] band %i connected to JACK input channel %d\n", bandi, id);
		nsk_bands[bandi].buffer = malloc(sizeof(float) * nsk__buffersize);
		if (nsk_bands[bandi].buffer == NULL) {
			printf("[audio] memory error during band buffer allocation\n");
		}

		// set default envelope times
		float coeff = (float)exp(ENV_TC / (5.0f * nsk__samplerate * 0.001));
		nsk_bands[bandi].env.attack_coeff = coeff;
		coeff = (float)exp(ENV_TC / (50.0f * nsk__samplerate * 0.001));
		nsk_bands[bandi].env.release_coeff = coeff;
	}

	nsk_bands[bandi].active = 1;
	nsk__last_selected_band = (unsigned)bandi;
	nsk_bands[bandi].use_hpf = 0;
	nsk_bands[bandi].use_lpf = 0;
	return 0;
}

static int nsk_band_name(lua_State * L)
{
	int rc = nsk__jack_open();
	if (rc != 0) {
		fprintf(stderr, "[audio] error opening JACK\n");
		return 0;
	}

	int bandi = (int)luaL_checkinteger(L, 1);
	const char * portname = luaL_checkstring(L, 2);

	if (bandi < 0 || bandi > NSK_MAX_BANDS) {
		printf("[audio] band id %d is exceeding max (%d)\n", bandi, NSK_MAX_BANDS);
		return 0;
	}

	if (!nsk_bands[bandi].active &&
	    nsk__jack_is_open &&
	    //nsk__jack_is_connected == NSK_JACK_DISCONNECTED &&
	    nsk_bands[bandi].port == NULL) {

		char inputname[] = "nsketch_x";
		inputname[8] = (char)(nsk__assigned_bands + (unsigned)'0');
		nsk__assigned_bands++;

		nsk_bands[bandi].port = jack_port_register(nsk__jack_client,
		                                           inputname,
		                                           JACK_DEFAULT_AUDIO_TYPE,
		                                           JackPortIsInput, 0);

		if (nsk_bands[bandi].port == NULL) {
			fprintf(stderr, "[audio] no more JACK ports available\n");
			return -1;
		}

		const char ** ports;
		ports = jack_get_ports(nsk__jack_client, portname,
		                       NULL, JackPortIsOutput);

		if (ports == NULL) {
			fprintf(stderr,
			        "[audio] no ports found for connecing band %d to \"%s\"\n",
			        bandi, portname);
			jack_free(ports);
			return 0;
		}

		if (jack_connect(nsk__jack_client, ports[0],
		                 jack_port_name(nsk_bands[bandi].port))) {
			fprintf(stderr, "[audio] cannot connect band %d to JACK output port \"%s\"\n", bandi, portname);
			jack_free(ports);
			return 0;
		}

		nsk__jack_is_connected = NSK_JACK_CONNECTED;
		jack_free(ports);
		printf("[audio] band %i connected to JACK output port \"%s\"\n", bandi, portname);

		nsk_bands[bandi].buffer = malloc(sizeof(float) * nsk__buffersize);
		if (nsk_bands[bandi].buffer == NULL) {
			printf("[audio] memory error during band buffer allocation\n");
		}

		// set default envelope times
		float coeff = (float)exp(ENV_TC / (5.0f * nsk__samplerate * 0.001));
		nsk_bands[bandi].env.attack_coeff = coeff;
		coeff = (float)exp(ENV_TC / (50.0f * nsk__samplerate * 0.001));
		nsk_bands[bandi].env.release_coeff = coeff;
	}

	nsk_bands[bandi].active = 1;
	nsk__last_selected_band = (unsigned)bandi;
	nsk_bands[bandi].use_hpf = 0;
	nsk_bands[bandi].use_lpf = 0;
	return 0;
}

static int nsk_audio_select(lua_State * L)
{
	int bandi = (int)luaL_checkinteger(L, 1);
	if (bandi < 0 || bandi > NSK_MAX_BANDS) {
		printf("[audio] band id %d is exceeding max (%d)\n",
		       bandi, NSK_MAX_BANDS);
		return 0;
	}
	nsk__last_selected_band = (unsigned)bandi;
	return 0;
}

static int nsk_audio_get(lua_State * L)
{
	int bandi = (int)luaL_optinteger(L, 1, nsk__last_selected_band);
	if (bandi < 0 || bandi > NSK_MAX_BANDS) {
		printf("[audio] band id %d is exceeding max (%d)\n",
		       bandi, NSK_MAX_BANDS);
		return 0;
	}
	lua_pushnumber(L, (lua_Number)nsk_bands[bandi].peak);
	return 1;
}

static int nsk_audio_get_db(lua_State * L)
{
	int bandi = (int)luaL_optinteger(L, 1, nsk__last_selected_band);
	if (bandi < 0 || bandi > NSK_MAX_BANDS) {
		printf("[audio] band id %d is exceeding max (%d)\n",
		       bandi, NSK_MAX_BANDS);
		return 0;
	}
	float lin = nsk_bands[bandi].peak;

	float db = -130.0f;
	if (lin != 0.0f) {
		db = 20.0f * log10f(lin);
	}
	lua_pushnumber(L, (lua_Number)db);
	return 1;
}

static int nsk_audio_highpass(lua_State * L)
{
	double freq = (double)luaL_checknumber(L, 1);
	double fc = freq / nsk__samplerate;
	double b1 = exp(-M_TWO_PI * fc);
	nsk_bands[nsk__last_selected_band].hpfc.a0 = (float)(1.0 - b1);
	nsk_bands[nsk__last_selected_band].hpfc.b1 = (float)b1;
	nsk_bands[nsk__last_selected_band].use_hpf = 1;
	return 0;
}

static int nsk_audio_lowpass(lua_State * L)
{
	double freq = (double)luaL_checknumber(L, 1);
	double fc = freq / nsk__samplerate;
	double b1 = exp(-M_TWO_PI * fc);
	nsk_bands[nsk__last_selected_band].lpfc.a0 = (float)(1.0 - b1);
	nsk_bands[nsk__last_selected_band].lpfc.b1 = (float)b1;
	nsk_bands[nsk__last_selected_band].use_lpf = 1;
	return 0;
}

static int nsk_audio_attack(lua_State * L)
{
	double time = (double)luaL_checknumber(L, 1);
	float coeff = (float)exp(ENV_TC / (time * nsk__samplerate * 0.001));
	nsk_bands[nsk__last_selected_band].env.attack_coeff = coeff;
	return 0;
}

static int nsk_audio_release(lua_State * L)
{
	double time = (double)luaL_checknumber(L, 1);
	float coeff = (float)exp(ENV_TC / (time * nsk__samplerate * 0.001));
	nsk_bands[nsk__last_selected_band].env.release_coeff = coeff;
	return 0;
}

// ------------------------------------------------------------------------------

static const luaL_Reg audio_namespace[] = {
    {"band_to_id", nsk_band_id},
    {"band_to_name", nsk_band_name},
    {"select", nsk_audio_select},
    {"get", nsk_audio_get},
    {"db", nsk_audio_get_db},
    {"lowpass", nsk_audio_lowpass},
    {"highpass", nsk_audio_highpass},
    {"attack", nsk_audio_attack},
    {"release", nsk_audio_release},
    {NULL, NULL}};

LUALIB_API int luaopen_audio(lua_State * L)
{
	luaL_register(L, "audio", audio_namespace);
	return 1;
}
