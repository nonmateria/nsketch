/* V4L2 video picture grabber
   Copyright (C) 2009 Mauro Carvalho Chehab <mchehab@infradead.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 */

/* 
	code tweaked by Nicola Pisanti for inclusion in nsketch
*/

#include "namespaces.h"

#include "rlgl.h"

#include <errno.h>
#include <fcntl.h>
#include <libv4l2.h>
#include <linux/videodev2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/types.h>
#define CLEAR(x) memset(&(x), 0, sizeof(x))

#include <pthread.h>
#define NSK_CAMBUFFERS 4

struct buffer {
	void * start;
	size_t length;
};

typedef struct nsk_webcam_t {
	// v4l2 stuff
	struct v4l2_format fmt;
	struct v4l2_buffer buf;
	struct v4l2_requestbuffers req;
	enum v4l2_buf_type type;
	fd_set fds;
	int fd;
	unsigned n_buffers;
	char * device;
	struct buffer * buffers;

	pthread_t thread;
	_Atomic int should_run;
	_Atomic int write;

	// raylib sutff
	size_t image_size;
	Image images[NSK_CAMBUFFERS];
	Texture tex2d;

	unsigned width;
	unsigned height;
} nsk_webcam_t;

static int xioctl(int fh, long unsigned request, void * arg)
{
	int r;

	do {
		r = v4l2_ioctl(fh, request, arg);
	} while (r == -1 && ((errno == EINTR) || (errno == EAGAIN)));

	if (r == -1) {
		fprintf(stderr, "[cam] error %d, %s\n", errno, strerror(errno));
		return 1;
	}

	return 0;
}

void nsk_webcam_close(nsk_webcam_t * cam)
{
	cam->should_run = 0;
	pthread_join(cam->thread, NULL);

	cam->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	xioctl(cam->fd, VIDIOC_STREAMOFF, &cam->type);
	for (unsigned i = 0; i < cam->n_buffers; ++i) {
		v4l2_munmap(cam->buffers[i].start, cam->buffers[i].length);
	}
	v4l2_close(cam->fd);

	free(cam->buffers);
	free(cam->device);

	for (unsigned i = 0; i < NSK_CAMBUFFERS; ++i) {
		UnloadImage(cam->images[i]);
	}
	UnloadTexture(cam->tex2d);
}

/* thread function */
void * nsk_webcam_threaded_func(void * arg)
{
	nsk_webcam_t * cam = (nsk_webcam_t *)arg;

	while (cam->should_run) {
		int r;
		struct timeval tv;

		do {
			FD_ZERO(&cam->fds);
			FD_SET(cam->fd, &cam->fds);

			/* Timeout. */
			tv.tv_sec = 2;
			tv.tv_usec = 0;

			r = select(cam->fd + 1, &cam->fds, NULL, NULL, &tv);
		} while ((r == -1 && (errno = EINTR)));

		if (r == -1) {
			perror("select");
			//return errno;
		}

		CLEAR(cam->buf);
		cam->buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		cam->buf.memory = V4L2_MEMORY_MMAP;
		xioctl(cam->fd, VIDIOC_DQBUF, &cam->buf);

		int w = cam->write;
		w++;
		if (w == NSK_CAMBUFFERS) {
			w = 0;
		}
		cam->write = w;
		// Copy required data to image
		memmove(cam->images[w].data, cam->buffers[cam->buf.index].start,
		        cam->image_size);
		xioctl(cam->fd, VIDIOC_QBUF, &cam->buf);
	}
	printf("[cam] closing cam thread\n");
	pthread_exit(NULL);
}

int nsk_webcam_open(nsk_webcam_t * cam, const char * device, unsigned w, unsigned h)
{
	cam->fd = -1;

	size_t sz = strlen(device) + 1;
	cam->device = malloc(sz);
	memmove(cam->device, device, sz);

	cam->fd = v4l2_open(cam->device, O_RDWR | O_NONBLOCK, 0);
	if (cam->fd < 0) {
		perror("[cam] Cannot open device");
		goto error;
	}

	cam->width = w;
	cam->height = h;
	cam->write = 0;

	CLEAR(cam->fmt);
	cam->fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	cam->fmt.fmt.pix.width = w;
	cam->fmt.fmt.pix.height = h;
	cam->fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
	cam->fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;
	if (xioctl(cam->fd, VIDIOC_S_FMT, &cam->fmt) != 0) {
		goto error;
	}
	if (cam->fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_RGB24) {
		printf("[cam] libv4l didn't accept RGB24 format. Can't proceed.\\n");
		goto error;
	}
	if ((cam->fmt.fmt.pix.width != w) || (cam->fmt.fmt.pix.height != h)) {
		printf("[cam] warning: driver is sending image at %dx%d \n",
		       cam->fmt.fmt.pix.width, cam->fmt.fmt.pix.height);
	}

	CLEAR(cam->req);
	cam->req.count = 2;
	cam->req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	cam->req.memory = V4L2_MEMORY_MMAP;

	if (xioctl(cam->fd, VIDIOC_REQBUFS, &cam->req) != 0) {
		goto error;
	}

	cam->buffers = calloc(cam->req.count, sizeof(*(cam->buffers)));

	unsigned i, n_buffers;

	for (n_buffers = 0; n_buffers < cam->req.count; ++n_buffers) {
		CLEAR(cam->buf);

		cam->buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		cam->buf.memory = V4L2_MEMORY_MMAP;
		cam->buf.index = n_buffers;
		if (xioctl(cam->fd, VIDIOC_QUERYBUF, &cam->buf) != 0) {
			goto error;
		}

		cam->buffers[n_buffers].length = cam->buf.length;
		cam->buffers[n_buffers].start = v4l2_mmap(NULL, cam->buf.length,
		                                          PROT_READ | PROT_WRITE,
		                                          MAP_SHARED,
		                                          cam->fd, cam->buf.m.offset);

		if (MAP_FAILED == cam->buffers[n_buffers].start) {
			perror("mmap");
			goto error;
		}
	}
	cam->n_buffers = n_buffers;

	for (i = 0; i < n_buffers; ++i) {
		CLEAR(cam->buf);
		cam->buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		cam->buf.memory = V4L2_MEMORY_MMAP;
		cam->buf.index = i;
		if (xioctl(cam->fd, VIDIOC_QBUF, &cam->buf) != 0) {
			goto error;
		}
	}
	cam->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (xioctl(cam->fd, VIDIOC_STREAMON, &cam->type) != 0) {
		goto error;
	}

	cam->image_size = (size_t)GetPixelDataSize((int)w, (int)h,
	                                           PIXELFORMAT_UNCOMPRESSED_R8G8B8);
	for (i = 0; i < NSK_CAMBUFFERS; ++i) {
		cam->images[i].data = RL_MALLOC(cam->image_size);
		cam->images[i].width = (int)w;
		cam->images[i].height = (int)h;
		cam->images[i].mipmaps = 1;
		cam->images[i].format = PIXELFORMAT_UNCOMPRESSED_R8G8B8;
	}
	cam->tex2d = LoadTextureFromImage(cam->images[0]);

	printf("[cam] starting cam thread for %s\n", device);
	cam->should_run = 1;
	int rc = pthread_create(&cam->thread, NULL, nsk_webcam_threaded_func, cam);
	if (rc != 0) {
		goto error;
	}

	return 0;
error:
	nsk_webcam_close(cam);
	return 1;
}

void nsk_webcam_update(nsk_webcam_t * cam)
{
	int read = cam->write;
	UpdateTexture(cam->tex2d, cam->images[read].data);
}

// ------------------------------------------------------------------------------

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

nsk_webcam_t nsk__cam;
int nsk__cam_is_open = 0;

void nsk_cam_update(void)
{
	if (nsk__cam_is_open) {
		nsk_webcam_update(&nsk__cam);
	}
}

void nsk_cam_release(void)
{
	if (nsk__cam_is_open) {
		nsk_webcam_close(&nsk__cam);
		nsk__cam_is_open = 0;
	}
}

// ------------------------------------------------------------------------------

static int cam_open(lua_State * L)
{
	const char * dev = luaL_checkstring(L, 1);
	unsigned w = (unsigned)luaL_checkinteger(L, 2);
	unsigned h = (unsigned)luaL_checkinteger(L, 3);

	// reopen cam if parameters are different
	if (nsk__cam_is_open && (w != nsk__cam.width ||
	                         h != nsk__cam.height ||
	                         (strcmp(dev, nsk__cam.device) != 0))) {
		nsk_webcam_close(&nsk__cam);
		nsk__cam_is_open = 0;
		printf("[cam] reopening cam\n");
	}

	if (!nsk__cam_is_open) {
		nsk_webcam_open(&nsk__cam, dev, w, h);
		nsk__cam_is_open = 1;
	}
	return 0;
}

static int cam_draw(lua_State * L)
{
	if (nsk__cam_is_open) {
		int x = (int)luaL_optinteger(L, 1, 0);
		int y = (int)luaL_optinteger(L, 2, 0);
		DrawTexture(nsk__cam.tex2d, x, y, WHITE);
	}
	return 0;
}

static int cam_uniform(lua_State * L)
{
	const char * uni_name = luaL_checkstring(L, 1);

	Shader shader = nsk_get_shader();

	if (nsk__cam_is_open) {
		int loc = GetShaderLocation(shader, uni_name);

		if (loc != -1) {
			Texture tex = nsk__cam.tex2d;
			SetShaderValueTexture(shader, loc, tex);
		}
		return 0;
	}
	printf("[frag] cam_texture() : no cam opened\n");
	return 0;
}

// ------------------------------------------------------------------------------

static const luaL_Reg cam_namespace[] = {
    {"open", cam_open},
    {"draw", cam_draw},
    {"uniform", cam_uniform},
    {NULL, NULL}};

LUALIB_API int luaopen_cam(lua_State * L)
{
	luaL_register(L, "cam", cam_namespace);
	return 1;
}
