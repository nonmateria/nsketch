
#include "namespaces.h"

#include "rlgl.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NSK_MAX_LAYERS 128
#define NSK_MAXNAMECHARS 32

typedef struct nsk_layer_t {
	RenderTexture fbos[2];
	int fi; // fbo index
	float off_x;
	float off_y;
	int w;
	int h;
	float scale;
	int show;
	int allocated;
	BlendMode blendmode;
	char name[NSK_MAXNAMECHARS]; // layers with a name are already loaded
} nsk_layer_t;

static nsk_layer_t layers[NSK_MAX_LAYERS];
static unsigned nsk__layers_size = 0;
static unsigned nsk__li = 0;
static int nsk__layer_is_open = 0;

static unsigned nsk__defw = 0;
static unsigned nsk__defh = 0;

static Color layer__color;

// ------------------------------------------------------------------------------

void nsk_layer_init(void)
{
	layer__color.r = 255;
	layer__color.g = 255;
	layer__color.b = 255;
	layer__color.a = 255;
	for (unsigned i = 0; i < NSK_MAX_LAYERS; ++i) {
		layers[i].off_x = 0;
		layers[i].off_y = 0;
		layers[i].w = 0;
		layers[i].h = 0;
		layers[i].scale = 0.0f;
		layers[i].show = 0;
		memset(layers[i].name, 0, NSK_MAXNAMECHARS);
		layers[i].fi = 0;
		layers[i].allocated = 0;
	}
}

void nsk_set_default_sizes(unsigned w, unsigned h)
{
	nsk__defw = w;
	nsk__defh = h;
}

void nsk_layer_reset(void)
{
	layer__color.r = 255;
	layer__color.g = 255;
	layer__color.b = 255;
	layer__color.a = 255;
	for (unsigned i = 0; i < NSK_MAX_LAYERS; ++i) {
		layers[i].show = 0;
		if (layers[i].allocated) {
			SetTextureFilter(layers[i].fbos[0].texture, TEXTURE_FILTER_POINT);
			SetTextureFilter(layers[i].fbos[1].texture, TEXTURE_FILTER_POINT);
			layers[i].blendmode = BLEND_ALPHA;
		}
	}
}

void nsk_layer_render(unsigned char alpha)
{
	Color tint = {.r = 255, .g = 255, .b = 255, .a = alpha};
	for (unsigned i = 0; i < NSK_MAX_LAYERS; ++i) {
		if (layers[i].show) {
			int fi = layers[i].fi;
			BeginBlendMode(layers[i].blendmode);

			float m = layers[i].scale;

			if (m != 0.0f) {
				rlPushMatrix();
				rlScalef(m, m, 1.0f);
				float w = (float)layers[i].fbos[fi].texture.width;
				float h = (float)-layers[i].fbos[fi].texture.height;
				float x = layers[i].off_x / m;
				float y = layers[i].off_y / m;
				DrawTextureRec(layers[i].fbos[fi].texture,
				               (Rectangle){0, 0, w, h},
				               (Vector2){x, y}, tint);
				rlPopMatrix();
			} else {
				float w = (float)layers[i].fbos[fi].texture.width;
				float h = (float)-layers[i].fbos[fi].texture.height;
				DrawTextureRec(layers[i].fbos[fi].texture,
				               (Rectangle){0, 0, w, h},
				               (Vector2){layers[i].off_x, layers[i].off_y}, tint);
			}
		}
	}
}

void nsk_layer_release(void)
{
	for (unsigned i = 0; i < NSK_MAX_LAYERS; ++i) {
		if (layers[i].allocated) {
			UnloadRenderTexture(layers[i].fbos[0]);
			UnloadRenderTexture(layers[i].fbos[1]);
			layers[i].allocated = 0;
		}
	}
}

RenderTexture nsk_layer_get_current_fbo(void)
{
	return layers[nsk__li].fbos[layers[nsk__li].fi];
}

RenderTexture nsk_layer_get_other_fbo(void)
{
	unsigned other = !layers[nsk__li].fi;
	return layers[nsk__li].fbos[other];
}

void nsk_layer_swap_fbos(void)
{
	layers[nsk__li].fi = !layers[nsk__li].fi;
}

void nsk_layer_open(void)
{
	if (!nsk__layer_is_open) {
		BeginTextureMode(layers[nsk__li].fbos[layers[nsk__li].fi]);
		nsk__layer_is_open = 1;
	}
}

void nsk_layer_close(void)
{
	if (nsk__layer_is_open) {
		EndTextureMode();
		nsk__layer_is_open = 0;
	}
}

// ------------------------------------------------------------------------------
static int layer_color(lua_State * L)
{
	unsigned r = (unsigned)luaL_checkinteger(L, 1);
	unsigned g = (unsigned)luaL_checkinteger(L, 2);
	unsigned b = (unsigned)luaL_checkinteger(L, 3);
	unsigned a = (unsigned)luaL_checkinteger(L, 4);

	unsigned char cr = (unsigned char)(r & 0xff);
	unsigned char cg = (unsigned char)(g & 0xff);
	unsigned char cb = (unsigned char)(b & 0xff);
	unsigned char ca = (unsigned char)(a & 0xff);

	layer__color.r = cr;
	layer__color.g = cg;
	layer__color.b = cb;
	layer__color.a = ca;

	return 0;
}

static int layer_create(lua_State * L)
{
	const char * name = luaL_checkstring(L, 1);
	int w = (int)luaL_optinteger(L, 2, nsk__defw);
	int h = (int)luaL_optinteger(L, 3, nsk__defh);
	float scale = (float)luaL_optnumber(L, 4, 0.0f);

	if (strlen(name) > NSK_MAXNAMECHARS) {
		printf("[layer] name can't be longer than %d chars.\n", NSK_MAXNAMECHARS);
		return 0;
	}

	unsigned i = 0;
	unsigned found = 0;

	for (; i < NSK_MAX_LAYERS; ++i) {
		if (layers[i].name != NULL &&
		    (name == layers[i].name || (strcmp(name, layers[i].name) == 0))) {
			//printf("updating layer %s ...\n", name );
			found = 1;
			break;
		}
	}

	if (!found) {
		if (nsk__layers_size == NSK_MAX_LAYERS) {
			printf("[layer] the maximum numbers of available layers is %d,"
			       "aborting...",
			       NSK_MAX_LAYERS);
			return 0;
		}
		nsk__layers_size++;
		i = nsk__layers_size - 1;

		// TODO : double check this string management
		snprintf(layers[i].name, NSK_MAXNAMECHARS, "%s", name);

		for (int la = 0; la < 2; ++la) {
			layers[i].fbos[la] = LoadRenderTexture(w, h);
			BeginTextureMode(layers[i].fbos[la]);
			rlClearColor(0, 0, 0, 0);
			rlClearScreenBuffers();
			EndTextureMode();
		}
	} else {
		if (w != layers[i].w || h != layers[i].h || scale != layers[i].scale) {
			for (int la = 0; la < 2; ++la) {
				UnloadRenderTexture(layers[i].fbos[la]);
				layers[i].fbos[la] = LoadRenderTexture(w, h);
				BeginTextureMode(layers[i].fbos[la]);
				rlClearColor(0, 0, 0, 0);
				rlClearScreenBuffers();
				EndTextureMode();
			}
		}
	}

	layers[i].off_x = 0;
	layers[i].off_y = 0;
	layers[i].w = w;
	layers[i].h = h;
	layers[i].show = 1;
	layers[i].allocated = 1;
	layers[i].scale = scale;

	SetTextureFilter(layers[i].fbos[0].texture, TEXTURE_FILTER_POINT);
	SetTextureFilter(layers[i].fbos[1].texture, TEXTURE_FILTER_POINT);

	nsk_layer_close();

	nsk__li = i;

	nsk_layer_open();

	return 0;
}

static int layer_select(lua_State * L)
{
	nsk_layer_close();
	const char * name = luaL_checkstring(L, 1);
	for (unsigned i = 0; i < nsk__layers_size; ++i) {
		if (strcmp(name, layers[i].name) == 0) {
			nsk__li = i;
			nsk_layer_open();
			return 0;
		}
	}
	printf("[layer] no layer %s found\n", name);
	return 0;
}

static int layer_draw(lua_State * L)
{
	const char * name = luaL_checkstring(L, 1);
	float x = (float)luaL_checknumber(L, 2);
	float y = (float)luaL_checknumber(L, 3);
	float scale = (float)luaL_optnumber(L, 4, 0.0f);

	for (unsigned i = 0; i < nsk__layers_size; ++i) {
		if (strcmp(name, layers[i].name) == 0) {
			int fi = layers[i].fi;
			if (scale != 0.0f) {
				rlPushMatrix();
				rlScalef(scale, scale, 1.0f);
				float w = (float)layers[i].fbos[fi].texture.width;
				float h = (float)-layers[i].fbos[fi].texture.height;
				DrawTextureRec(layers[i].fbos[fi].texture,
				               (Rectangle){0, 0, w, h},
				               (Vector2){x / scale, y / scale}, layer__color);
				rlPopMatrix();
			} else {
				float w = (float)layers[i].fbos[fi].texture.width;
				float h = (float)-layers[i].fbos[fi].texture.height;
				DrawTextureRec(layers[i].fbos[fi].texture,
				               (Rectangle){0, 0, w, h},
				               (Vector2){x, y}, layer__color);
			}
			return 0;
		}
	}
	printf("[layer] no layer %s found\n", name);
	return 0;
}

static int layer_position(lua_State * L)
{
	layers[nsk__li].off_x = (float)luaL_checknumber(L, 1);
	layers[nsk__li].off_y = (float)luaL_checknumber(L, 2);
	return 0;
}

static int layer_scale(lua_State * L)
{
	layers[nsk__li].scale = (float)luaL_checknumber(L, 1);
	return 0;
}

static int layer_center(lua_State * L)
{
	layers[nsk__li].off_x = (float)(GetScreenWidth() - layers[nsk__li].w) * 0.5f;
	layers[nsk__li].off_y = (float)(GetScreenHeight() - layers[nsk__li].h) * 0.5f;
	return 0;
	(void)L;
}

static int layer_hide(lua_State * L)
{
	layers[nsk__li].show = 0;
	return 0;
	(void)L;
}

static int layer_show(lua_State * L)
{
	layers[nsk__li].show = 1;
	return 0;
	(void)L;
}

static int layer_warning(lua_State * L)
{
	printf("warning! deprecated open/close\n");
	return 0;
	(void)L;
}

static int layer_save(lua_State * L)
{
	nsk_layer_close();
	const char * dest_path = luaL_checkstring(L, 1);
	Image pixels = LoadImageFromTexture(layers[nsk__li].fbos[layers[nsk__li].fi].texture);
	ImageFlipVertical(&pixels);
	ExportImage(pixels, dest_path);
	UnloadImage(pixels);
	return 0;
}

static int layer_width(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)layers[nsk__li].w);
	return 1;
}

static int layer_height(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)layers[nsk__li].h);
	return 1;
}

static int layer_filter(lua_State * L)
{
	int mode = (int)luaL_checkinteger(L, 1);
	switch (mode) {
	case 0:
	case 1:
	default:
		SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_POINT);
		SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_POINT);
		break;
	case 2:
		SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_BILINEAR);
		SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_BILINEAR);
		break;
	case 3:
		SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_TRILINEAR);
		SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_TRILINEAR);
		break;
	case 4:
	case 5:
	case 6:
	case 7:
		SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_ANISOTROPIC_4X);
		SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_ANISOTROPIC_4X);
		break;
	case 8:
	case 9:
	case 10:
	case 11:
	case 12:
	case 13:
	case 14:
	case 15:
		SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_ANISOTROPIC_8X);
		SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_ANISOTROPIC_8X);
		break;
	case 16:
		SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_ANISOTROPIC_16X);
		SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_ANISOTROPIC_16X);
		break;
	}

	return 0;
}

static int layer_filter_nearest(lua_State * L)
{
	SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_POINT);
	SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_POINT);
	return 0;
	(void)L;
}

static int layer_filter_linear(lua_State * L)
{
	SetTextureFilter(layers[nsk__li].fbos[0].texture, TEXTURE_FILTER_BILINEAR);
	SetTextureFilter(layers[nsk__li].fbos[1].texture, TEXTURE_FILTER_BILINEAR);
	return 0;
	(void)L;
}

static int layer_blend_alpha(lua_State * L)
{
	layers[nsk__li].blendmode = BLEND_ALPHA;
	return 0;
	(void)L;
}

static int layer_blend_additive(lua_State * L)
{
	layers[nsk__li].blendmode = BLEND_ADDITIVE;
	return 0;
	(void)L;
}

static int layer_blend_multiply(lua_State * L)
{
	layers[nsk__li].blendmode = BLEND_MULTIPLIED;
	return 0;
	(void)L;
}

static int layer_clear(lua_State * L)
{
	unsigned r = (unsigned)luaL_checkinteger(L, 1);
	unsigned g = (unsigned)luaL_checkinteger(L, 2);
	unsigned b = (unsigned)luaL_checkinteger(L, 3);
	unsigned a = (unsigned)luaL_checkinteger(L, 4);

	unsigned char cr = (unsigned char)(r & 0xff);
	unsigned char cg = (unsigned char)(g & 0xff);
	unsigned char cb = (unsigned char)(b & 0xff);
	unsigned char ca = (unsigned char)(a & 0xff);

	rlClearColor(cr, cg, cb, ca);
	rlClearScreenBuffers();
	return 0;
}

static int layer_uniform(lua_State * L)
{
	int opt = lua_gettop(L);
	if (opt < 2) {
		printf("[layer] not enought values for layer.uniform()!\n");
		return 0;
	}
	const char * uni_name = luaL_checkstring(L, 1);
	const char * uni_source = luaL_checkstring(L, 2);

	Shader shader = nsk_get_shader();

	int loc = GetShaderLocation(shader, uni_name);

	if (loc != -1) {
		for (unsigned i = 0; i < nsk__layers_size; ++i) {
			if (strcmp(uni_source, layers[i].name) == 0) {
				Texture tex = layers[i].fbos[layers[i].fi].texture;
				SetShaderValueTexture(shader, loc, tex);
				return 0;
			}
		}
		printf("[layer] layer.uniform() no layer %s found\n", uni_source);
	}

	return 0;
}
// ------------------------------------------------------------------------------
static const luaL_Reg layer_namespace[] = {
    {"create", layer_create},
    {"select", layer_select},
    {"draw", layer_draw},
    {"center", layer_center},
    {"position", layer_position},
    {"scale", layer_scale},
    {"filter", layer_filter},
    {"filter_nearest", layer_filter_nearest},
    {"filter_linear", layer_filter_linear},
    {"blend_alpha", layer_blend_alpha},
    {"blend_additive", layer_blend_additive},
    {"blend_multiply", layer_blend_multiply},
    {"hide", layer_hide},
    {"show", layer_show},
    {"open", layer_warning},
    {"close", layer_warning},
    {"save", layer_save},
    {"width", layer_width},
    {"height", layer_height},
    {"color", layer_color},
    {"clear", layer_clear},
    {"uniform", layer_uniform},
    {NULL, NULL}};

LUALIB_API int luaopen_layer(lua_State * L)
{
	luaL_register(L, "layer", layer_namespace);

	lua_pushinteger(L, 1);
	lua_setfield(L, -2, "nearest");

	lua_pushinteger(L, 2);
	lua_setfield(L, -2, "bilinear");

	lua_pushinteger(L, 3);
	lua_setfield(L, -2, "trilinear");

	lua_pushinteger(L, 4);
	lua_setfield(L, -2, "anisotropic_4x");

	lua_pushinteger(L, 8);
	lua_setfield(L, -2, "anisotropic_8x");

	lua_pushinteger(L, 16);
	lua_setfield(L, -2, "anisotropic_16x");

	return 1;
}
