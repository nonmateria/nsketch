#pragma once

#ifndef F_TWO_PI
#define F_TWO_PI 6.2831853071795864769252867665590f
#endif

#ifndef F_PI
#define F_PI 3.1415926535897932384626433832795f
#endif

int nsk_get_name_from_path(const char * path, char * buffer, unsigned buffersize);

int nsk_path_is_lua_file(const char * path, unsigned len);

int nsk_match_ext(const char * path, const char * ext);
