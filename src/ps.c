
#include "namespaces.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#include "3rdp/simplexnoise1234.h"

static inline float fast_sqrt(float x)
{
	float xhalf = 0.5f * x;
	int i = *(int *)&x;             // store floating-point bits in integer
	i = 0x5f3759df - (i >> 1);      // initial guess for Newton's method
	x = *(float *)&i;               // convert new bits into float
	x = x * (1.5f - xhalf * x * x); // One round of Newton's method
	return 1.0f / x;
}

// ---- vector functions --------------------------------------------------------
typedef struct vec2_t {
	float x;
	float y;
} vec2_t;

static inline vec2_t vec2_add(vec2_t v1, vec2_t v2)
{
	vec2_t result = {v1.x + v2.x, v1.y + v2.y};
	return result;
}

static inline vec2_t vec2_add_value(vec2_t v, float val)
{
	vec2_t result = {v.x + val, v.y + val};
	return result;
}
static inline vec2_t vec2_sub(vec2_t v1, vec2_t v2)
{
	vec2_t result = {v1.x - v2.x, v1.y - v2.y};
	return result;
}

static inline vec2_t vec2_sub_value(vec2_t v, float val)
{
	vec2_t result = {v.x - val, v.y - val};
	return result;
}

static inline vec2_t vec2_mult(vec2_t v1, vec2_t v2)
{
	vec2_t result = {v1.x * v2.x, v1.y * v2.y};
	return result;
}

static inline vec2_t vec2_div(vec2_t v1, vec2_t v2)
{
	vec2_t result = {v1.x / v2.x, v1.y / v2.y};
	return result;
}

static inline vec2_t vec2_scale(vec2_t v, float val)
{
	vec2_t result = {v.x * val, v.y * val};
	return result;
}

static inline vec2_t vec2_neg(vec2_t v)
{
	vec2_t result = {-v.x, -v.y};
	return result;
}

static inline float vec2_length(vec2_t v)
{
	float result = fast_sqrt((v.x * v.x) + (v.y * v.y));
	return result;
}

static inline float vec2_length_sqr(vec2_t v)
{
	float result = (v.x * v.x) + (v.y * v.y);
	return result;
}

static inline float vec2_dot(vec2_t v1, vec2_t v2)
{
	float result = (v1.x * v2.x + v1.y * v2.y);
	return result;
}

static inline float vec2_distance(vec2_t v1, vec2_t v2)
{
	float result = fast_sqrt((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y));
	return result;
}

static inline vec2_t vec2_norm(vec2_t v)
{
	vec2_t result = vec2_scale(v, 1.0f / vec2_length(v));
	return result;
}

static inline vec2_t vec2_lerp(vec2_t v1, vec2_t v2, float amount)
{
	vec2_t result = {0};
	result.x = v1.x + amount * (v2.x - v1.x);
	result.y = v1.y + amount * (v2.y - v1.y);
	return result;
}

// Calculate reflected vector to normal
static inline vec2_t vec2_reflect(vec2_t v, vec2_t normal)
{
	vec2_t result = {0};
	float dot_product = vec2_dot(v, normal);
	result.x = v.x - (2.0f * normal.x) * dot_product;
	result.y = v.y - (2.0f * normal.y) * dot_product;
	return result;
}

// ------------------------------------------------------------------------------
static unsigned nsk__max_particles = 0;
static unsigned nsk_ps_size = 0;
static float nsk__lifespan = 60.0f;

static float nsk__dt = (float)(1.0 / 60.0);
static float nsk__drag = 0.9f;
static float nsk__speed = 1.0f;
static float nsk__spawnrand = 10.0f;

static float ps__minx = -42000.0f;
static float ps__maxx = 42000.0f;
static float ps__miny = -42000.0f;
static float ps__maxy = 42000.0f;

typedef struct ps__particle {
	vec2_t position;
	vec2_t velocity;
	vec2_t acceleration;
	vec2_t old_position;
	float mass;
	float life;
} particle_t;

static particle_t * nsk_ps = NULL;

static float * nsk_vectors = NULL;
static int nsk_nw = 0;
static int nsk_nh = 0;
static int nsk_nstep = 0;

static Color nsk__color;

void nsk_ps_set_defaults(void)
{
	nsk__color.r = 255;
	nsk__color.g = 255;
	nsk__color.b = 255;
	nsk__color.a = 255;
}

void nsk_ps_release(void)
{
	if (nsk_ps != NULL) {
		free(nsk_ps);
		nsk_ps = NULL;
	}
	if (nsk_vectors != NULL) {
		free(nsk_vectors);
		nsk_vectors = NULL;
	}
}

// ------------------------------------------------------------------------------
static int ps_color(lua_State * L)
{
	unsigned r = (unsigned)luaL_checkinteger(L, 1);
	unsigned g = (unsigned)luaL_checkinteger(L, 2);
	unsigned b = (unsigned)luaL_checkinteger(L, 3);
	unsigned a = (unsigned)luaL_checkinteger(L, 4);

	unsigned char cr = (unsigned char)(r & 0xff);
	unsigned char cg = (unsigned char)(g & 0xff);
	unsigned char cb = (unsigned char)(b & 0xff);
	unsigned char ca = (unsigned char)(a & 0xff);

	nsk__color.r = cr;
	nsk__color.g = cg;
	nsk__color.b = cb;
	nsk__color.a = ca;

	return 0;
}

static int ps_setup_particles(lua_State * L)
{
	unsigned size = (unsigned)luaL_checkinteger(L, 1);
	if (size != nsk__max_particles) {
		if (nsk_ps != NULL) {
			free(nsk_ps);
		}
		nsk_ps = malloc(sizeof(particle_t) * size);
		if (nsk_ps == NULL) {
			fprintf(stderr, "[ps] ERROR during particles memory allocation!\n");
		} else {
			nsk_ps_size = 0;
			for (unsigned i = 0; i < nsk__max_particles; ++i) {
				nsk_ps[i].life = -0.1f; // expired
			}
			nsk__max_particles = size;
			printf("[ps] got memory for %d particles\n", size);
		}
	}
	return 0;
}

static int ps_add_particle(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);

	if (nsk_ps_size < nsk__max_particles) {
		unsigned i = nsk_ps_size;
		nsk_ps[i].life = nsk__lifespan;

		float randamt = nsk__spawnrand;
		float randamt2 = nsk__spawnrand * 2.0f;
		nsk_ps[i].position.x = x + nsk_range(-randamt2, randamt2);
		nsk_ps[i].position.y = y + nsk_range(-randamt2, randamt2);
		nsk_ps[i].mass = 1.0f + nsk_range(-0.005f * randamt, 0.005f * randamt);

		nsk_ps[i].velocity.x = nsk_range(-randamt, randamt);
		nsk_ps[i].velocity.y = nsk_range(-randamt, randamt);
		nsk_ps[i].acceleration.x = nsk_range(-randamt, randamt);
		nsk_ps[i].acceleration.y = nsk_range(-randamt, randamt);
		nsk_ps_size++;
	}
	return 0;
}

static int ps_update(lua_State * L)
{
	float dt = nsk__dt * nsk__speed;
	float dta = fabsf(dt);
	float drag = nsk__drag;

	for (unsigned i = 0; i < nsk_ps_size; ++i) {
		nsk_ps[i].old_position = nsk_ps[i].position;

		// a = (acceleration * dt) / mass
		// velocity += a;
		// acceleration -= a;
		vec2_t a = vec2_scale(nsk_ps[i].acceleration, dt / nsk_ps[i].mass);
		nsk_ps[i].velocity = vec2_add(nsk_ps[i].velocity, a);
		nsk_ps[i].acceleration = vec2_sub(nsk_ps[i].acceleration, a);

		// position += velocity * dt
		// velocity -= (velocity * dt * (1.0f - drag));
		vec2_t vdt = vec2_scale(nsk_ps[i].velocity, dt);
		vec2_t vdt_dragged = vec2_scale(vdt, drag);
		nsk_ps[i].position = vec2_add(nsk_ps[i].position, vdt);
		nsk_ps[i].velocity = vec2_sub(nsk_ps[i].velocity, vdt_dragged);

		nsk_ps[i].life -= dta;
	}

	for (unsigned i = 0; i < nsk_ps_size; ++i) {
		if (nsk_ps[i].life <= 0.0f || nsk_ps[i].position.x < ps__minx || nsk_ps[i].position.x > ps__maxx || nsk_ps[i].position.y < ps__miny || nsk_ps[i].position.y > ps__maxy) {
			nsk_ps[i] = nsk_ps[nsk_ps_size - 1];
			nsk_ps_size--;
		}
	}

	return 0;
	(void)L;
}

void ps__attraction_force(float x, float y, float strength)
{
	float scale = strength * nsk__dt * nsk__speed;

	unsigned max = nsk_ps_size;
	for (unsigned i = 0; i < max; ++i) {
		vec2_t dir = {x - nsk_ps[i].position.x, y - nsk_ps[i].position.y};
		dir = vec2_norm(dir);
		dir = vec2_scale(dir, scale);
		nsk_ps[i].acceleration = vec2_add(nsk_ps[i].acceleration, dir);
	}
}

void ps__attraction_force_ranged(float x, float y, float strength, float max)
{
	float scale = strength * nsk__dt * nsk__speed;

	for (unsigned i = 0; i < nsk_ps_size; ++i) {
		float dist = vec2_distance((vec2_t){x, y}, nsk_ps[i].position);
		if (dist < max) {
			vec2_t dir = {x - nsk_ps[i].position.x, y - nsk_ps[i].position.y};
			dir = vec2_norm(dir);
			dir = vec2_scale(dir, scale);
			nsk_ps[i].acceleration = vec2_add(nsk_ps[i].acceleration, dir);
		}
	}
}

static int ps_attract(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float a = (float)luaL_checknumber(L, 3);
	ps__attraction_force(x, y, a);
	return 0;
}

static int ps_repulse(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float a = (float)luaL_checknumber(L, 3);
	ps__attraction_force(x, y, -a);
	return 0;
}

static int ps_attract_ranged(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float a = (float)luaL_checknumber(L, 3);
	float r = (float)luaL_checknumber(L, 4);
	ps__attraction_force_ranged(x, y, a, r);
	return 0;
}

static int ps_repulse_ranged(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float a = (float)luaL_checknumber(L, 3);
	float r = (float)luaL_checknumber(L, 4);
	ps__attraction_force_ranged(x, y, -a, r);
	return 0;
}

static int ps_rotate(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float a = (float)luaL_checknumber(L, 3);
	float dt = nsk__dt * nsk__speed;

	unsigned max = nsk_ps_size;
	for (unsigned i = 0; i < max; ++i) {
		vec2_t dir = {nsk_ps[i].position.y - y, x - nsk_ps[i].position.x};
		float dist = vec2_length(dir);
		dir = vec2_norm(dir);

		float scale = (a / dist) * dt;
		dir = vec2_scale(dir, scale);

		nsk_ps[i].acceleration = vec2_add(nsk_ps[i].acceleration, dir);
	}
	return 0;
}

static int ps_setup_vectors(lua_State * L)
{
	int w = (int)luaL_checkinteger(L, 1);
	int h = (int)luaL_checkinteger(L, 2);
	int step = (int)luaL_checkinteger(L, 3);

	if (nsk_nw != w || nsk_nh != h || nsk_nstep != step) {
		if (nsk_vectors != NULL) {
			free(nsk_vectors);
		}
		size_t init_size = sizeof(float) * (unsigned)(w * h * 2);
		nsk_vectors = malloc(init_size);
		memset(nsk_vectors, 0, init_size);

		if (nsk_vectors == NULL) {
			fprintf(stderr, "[ps] ERROR during vector field memory allocation!\n");
		} else {
			nsk_nw = w;
			nsk_nh = h;
			nsk_nstep = step;
			printf("[ps] got memory for a %dx%d vector field\n", w, h);
		}
	}
	return 0;
}

static int ps_vectors_force(lua_State * L)
{
	float a = (float)luaL_checknumber(L, 1);
	int w = nsk_nw;
	int h = nsk_nh;
	int step = nsk_nstep;

	if (w != 0 && h != 0) {
		unsigned max = nsk_ps_size;
		float dt = nsk__dt * nsk__speed;

		for (unsigned i = 0; i < max; ++i) {
			int x = (int)nsk_ps[i].position.x;
			int y = (int)nsk_ps[i].position.y;
			x /= step;
			y /= step;

			if (x >= w) {
				x = w - 1;
			}
			if (y >= h) {
				y = h - 1;
			}
			if (x < 0) {
				x = 0;
			}
			if (y < 0) {
				y = 0;
			}

			float scale = a * dt;

			int fi = x + y * w;
			vec2_t dir = {nsk_vectors[fi] * scale, nsk_vectors[fi + 1] * scale};

			nsk_ps[i].acceleration = vec2_add(nsk_ps[i].acceleration, dir);
		}
	} else {
		printf("[ps] warning! calling ps.vectorfield() without allocated vectorfield\n");
	}

	return 0;
}

static int ps_draw(lua_State * L)
{
	float maxlifedivide = 1.0f / nsk__lifespan;

	unsigned max = nsk_ps_size;
	for (unsigned i = 0; i < max; ++i) {
		float pct = nsk_ps[i].life * maxlifedivide;
		if (pct < 0.0f) {
			pct = 0.0f;
		}
		if (pct > 1.0f) {
			pct = 1.0f;
		}

		float alpha = 255.0f * pct;

		Color c = nsk__color;
		c.a = (unsigned char)alpha;
		float distx = fabsf(nsk_ps[i].position.x - nsk_ps[i].old_position.x);
		float disty = fabsf(nsk_ps[i].position.y - nsk_ps[i].old_position.y);
		if (distx > 1.0f || disty > 1.0f) {
			DrawLine((int)nsk_ps[i].old_position.x,
			         (int)nsk_ps[i].old_position.y,
			         (int)nsk_ps[i].position.x,
			         (int)nsk_ps[i].position.y, c);
		} else {
			DrawLine((int)nsk_ps[i].position.x,
			         (int)nsk_ps[i].position.y,
			         (int)nsk_ps[i].position.x,
			         (int)(nsk_ps[i].position.y) + 1, c);
		}
	}
	return 0;
	(void)L;
}

static int ps_noise_to_vectors(lua_State * L)
{
	float t = (float)luaL_checknumber(L, 1);
	float mult = (float)luaL_checknumber(L, 2);

	int w = nsk_nw;
	int h = nsk_nh;
	float range = F_TWO_PI * 2.0;

	for (int y = 0; y < h; ++y) {
		for (int x = 0; x < w; ++x) {
			float xx = (mult * (float)x) / (float)w;
			float yy = (mult * (float)y) / (float)w;
			float angle = snoise3(xx, yy, t) * range;

			vec2_t dir = {cosf(angle), sinf(angle)};
			dir = vec2_norm(dir);
			dir = vec2_scale(dir, snoise3(xx, yy, t + 10.0f) * 0.5f + 0.5f);

			int i = x + y * w;
			nsk_vectors[i] = dir.x;
			nsk_vectors[i + 1] = dir.y;
		}
	}

	return 0;
}

// this is for debugging the noise flow
static int ps_draw_vectorfield(lua_State * L)
{
	int w = nsk_nw;
	int h = nsk_nh;
	int step = nsk_nstep;

	if (w != 0 && h != 0) {
		Color c = {.r = 0, .g = 0, .b = 40, .a = 255};
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {
				int x0 = x * step;
				int y0 = y * step;

				int i = x + y * w;
				int x1 = x0 + (int)(nsk_vectors[i] * (float)step);
				int y1 = y0 + (int)(nsk_vectors[i + 1] * (float)step);

				DrawCircle(x0, y0, 1, c);
				DrawLine(x0, y0, x1, y1, c);
			}
		}
	} else {
		printf("[ps] warning! calling ps.draw_vectorfield() without allocated vectorfield\n");
	}

	return 0;
	(void)L;
}

static int ps_boundaries(lua_State * L)
{
	ps__minx = (float)luaL_checknumber(L, 1);
	ps__miny = (float)luaL_checknumber(L, 2);
	ps__maxx = (float)luaL_checknumber(L, 3);
	ps__maxy = (float)luaL_checknumber(L, 4);
	return 0;
}

static int ps_drag(lua_State * L)
{
	nsk__drag = (float)luaL_checknumber(L, 1);
	return 0;
}

static int ps_lifespan(lua_State * L)
{
	nsk__lifespan = (float)luaL_checknumber(L, 1);
	return 0;
}

static int ps_speed(lua_State * L)
{
	nsk__speed = (float)luaL_checknumber(L, 1);
	return 0;
}

static int ps_spawn_randomize(lua_State * L)
{
	nsk__spawnrand = (float)luaL_checknumber(L, 1);
	return 0;
}

static int ps_get_particles_size(lua_State * L)
{
	lua_pushnumber(L, nsk_ps_size);
	return 1;
}

static int ps_set_vector(lua_State * L)
{
	int ix = (int)luaL_checkinteger(L, 1) - 1;
	int iy = (int)luaL_checkinteger(L, 2) - 1;
	float ax = (float)luaL_checknumber(L, 3);
	float ay = (float)luaL_checknumber(L, 4);
	int nw = nsk_nw;

	if (ix >= 0 && ix < nw && iy >= 0 && iy < nsk_nh) {
		int i = ix + iy * nw;
		nsk_vectors[i] = ax;
		nsk_vectors[i + 1] = ay;
	} else {
		printf("[ps] warning! trying to set vector out of range\n");
	}

	return 0;
}

static int ps_get_vectors_width(lua_State * L)
{
	lua_pushnumber(L, nsk_nw);
	return 1;
}

static int ps_get_vectors_height(lua_State * L)
{
	lua_pushnumber(L, nsk_nh);
	return 1;
}

// ------------------------------------------------------------------------------
static const luaL_Reg ps_namespace[] = {
    {"setup_particles", ps_setup_particles},
    {"drag", ps_drag},
    {"speed", ps_speed},
    {"boundaries", ps_boundaries},
    {"lifespan", ps_lifespan},
    {"spawn", ps_add_particle},
    {"spawn_randomize", ps_spawn_randomize},
    {"attract", ps_attract},
    {"attract_ranged", ps_attract_ranged},
    {"repulse", ps_repulse},
    {"repulse_ranged", ps_repulse_ranged},
    {"rotate", ps_rotate},
    {"update", ps_update},
    {"setup_vectors", ps_setup_vectors},
    {"vectors", ps_vectors_force},
    {"set_vector", ps_set_vector},
    {"noise_to_vectors", ps_noise_to_vectors},
    {"get_vectors_width", ps_get_vectors_width},
    {"get_vectors_heigh", ps_get_vectors_height},
    {"get_size", ps_get_particles_size},
    {"draw", ps_draw},
    {"draw_vectors", ps_draw_vectorfield},
    {"color", ps_color},
    {NULL, NULL}};

LUALIB_API int luaopen_ps(lua_State * L)
{
	luaL_register(L, "ps", ps_namespace);
	return 1;
}
