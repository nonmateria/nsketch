
#include "raylib.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "filewatcher.h"
#include "namespaces.h"

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

void quit_handler(int signo)
{
	if (signo == SIGINT || signo == SIGTERM) {
		printf("\ntrapped SIGINT or SIGTERM, quitting...\n");
		nsk_window_quit();
	}
}

int add_quit_handlers(void)
{
	if (signal(SIGINT, quit_handler) == SIG_ERR) {
		printf("error on assigning signal handler\n");
		return 2;
	}
	if (signal(SIGTERM, quit_handler) == SIG_ERR) {
		printf("error on assigning signal handler\n");
		return 2;
	}
	return 0;
}

int check_script(const char * path, char * script, int here);

lua_State * reload(lua_State * L, const char * file, int * error_report)
{
	if (L != NULL) {
		lua_close(L);
		L = NULL;
	}

	L = luaL_newstate(); // open Lua
	if (!L) {
		return NULL;
	}
	luaL_openlibs(L); // load Lua libraries

	luaopen_nsk(L);
	luaopen_window(L);
	luaopen_args(L);
	luaopen_audio(L);
	luaopen_cam(L);
	luaopen_layer(L);
	luaopen_frag(L);
	luaopen_osc(L);
	luaopen_png(L);
	luaopen_ps(L);
	luaopen_fn(L);
	luaopen_control(L);

	nsk_canvas_set_defaults();
	nsk_png_set_defaults();
	nsk_ps_set_defaults();

	// clear stack since opening libs leaves tables on the stack
	lua_settop(L, 0);

	// TODO set the panic function
	//lua_atpanic(L, &at_panic);

	// ---- reset namespaces ----
	nsk_layer_reset();

	//---------------------------

	int rc = luaL_loadfile(L, file); // load Lua script
	if (rc != 0) {
		*error_report = rc;
		printf("[loading] %s\n", lua_tostring(L, -1));
		return NULL;
	}

	rc = lua_pcall(L, 0, 0, 0); // tell Lua to run the script
	if (rc != 0) {
		printf("[syntax] %s\n", lua_tostring(L, -1));
	}

	*error_report = rc;

	return L;
}

int main(int argc, char ** argv)
{
	add_quit_handlers();
	int error_report = 1;
	lua_State * L = NULL;

	int here = 0;
	size_t path_size = 0;
	const char * path = NULL;
	if (argc < 2) {
		here = 1;
	} else {
		path_size = strlen(argv[1]) + 1;
		path = argv[1];
	}
	char * scriptpath = malloc(path_size + 9); // space to add "/main.lua"

	if (!check_script(path, scriptpath, here)) {
		return 1;
	}

	SetTraceLogLevel(LOG_WARNING);

	// ------ init namespaces --
	nsk_layer_init();
	nsk_frag_init();
	nsk_png_init();
	nsk_lfo_init();
	nsk_osc_init();
	nsk_args_init(argc, argv);
	nsk_jack_init();

	// -------------------------

	filewatcher_t * main_watch = filewatcher_create(scriptpath);
	L = reload(L, scriptpath, &error_report);

	nsk_layer_close(); // reload will open a layer on "create"

	while (nsk_should_continue()) {
		if (WindowShouldClose()) {
			nsk_window_quit();
		}

		SetTraceLogLevel(LOG_WARNING);
		nsk_frag_watch();
		if (filewatcher_change_detected(main_watch)) {
			L = reload(L, scriptpath, &error_report);
		}

		SetTraceLogLevel(LOG_ERROR);

		nsk_cam_update();
		nsk_osc_update();

		float delta = nsk_clock_update();

		nsk_layer_open();
		if (L && error_report == 0) {
			lua_getglobal(L, "loop");
			error_report = lua_pcall(L, 0, 0, 0);
			if (error_report) {
				printf("%s\n", lua_tostring(L, -1));
			}
		}
		nsk_layer_close();

		// draw ---------
		BeginDrawing();
		unsigned char alpha = nsk_prepare_window(delta);
		nsk_layer_render(alpha);
		if (error_report != 0) {
			Color c = {.r = 100, .g = 0, .b = 0, .a = 255};
			BeginBlendMode(BLEND_ADD_COLORS);
			DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), c);
		}
		EndDrawing();
	}

	filewatcher_destroy(main_watch);
	free(scriptpath);

	lua_close(L);

	nsk_layer_release();
	nsk_frag_release();
	nsk_canvas_release();
	nsk_cam_release();
	nsk_png_release();
	nsk_ps_release();

	// CloseWindow will also automatically deallocate graphic resources
	// so it needs to be after those *_release() to avoid segfaults
	CloseWindow();

	nsk_osc_release();
	nsk_jack_close();

	return 0;
}

int check_script(const char * path, char * script, int here)
{
	int kind = 0; // not valid

	int len = 0;
	if (path != NULL) { // not using "here";
		len = (int)strlen(path);
	}

	if (here || (len == 1 && path[0] == '.')) {
		kind = 3; // checks for file here
	} else {
		for (int i = len - 1; i >= 0; i--) {
			if (path[i] == '.') {
				kind = 2; // file
				break;
			}
		}
		if (kind != 2) { // directory with or without trailing slash
			kind = 1;
		}
	}

	switch (kind) {

	case 0:
	default:
		printf("[loading] %s is not a correct path\n", path);
		break;

	case 1: { // checks directory for main.lua and chdir
		memmove(script, path, (size_t)len);
		int i = len;
		if (path[i - 1] != '/') {
			script[i] = '/';
			i++;
		}

		chdir(path);

		memmove(script, "main.lua\0", 9);

		if (access(script, F_OK) == -1) {
			printf("[loading] %s file not found\n", script);
			return 0;
		}

		return 1;
	} break;

	case 2: { // check files is a lua file, move to chdir
		if (path[len - 4] != '.' ||
		    path[len - 3] != 'l' ||
		    path[len - 2] != 'u' ||
		    path[len - 1] != 'a') {
			printf("[loading] input file is not a .lua file!\n");
			return 0;
		}

		int dirpoint = len - 5;
		for (; dirpoint > 0; dirpoint--) {
			if (path[dirpoint] == '/') {
				break;
			}
		}

		if (dirpoint != 0) { // if dirpoint==0 we are already in the right folder
			memmove(script, path, (size_t)dirpoint);
			script[dirpoint] = '\0'; // null terminated
			chdir(script);
			dirpoint++;

			int file_name_size = len - dirpoint;
			memmove(script, path + dirpoint, (size_t)file_name_size);
			script[file_name_size] = '\0';
		} else {
			memmove(script, path, (size_t)len);
		}

		return 1;
	} break;

	case 3: { // checks for files in current directory
		memmove(script, "main.lua\0", 9);

		if (access(script, F_OK) == -1) {
			printf("[loading] %s file not found\n", script);
			return 0;
		}
		return 1;
	} break;
	}

	return 0;
}
