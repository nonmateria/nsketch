
#include "namespaces.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char ** nsk__args;
static int nsk__count;

void nsk_args_init(int argc, char ** argv)
{
	nsk__args = argv;
	nsk__count = argc;
}

static int args_get(lua_State * L)
{
	int i = (int)luaL_checkinteger(L, 1);
	i += 2;
	if (i < nsk__count) {
		lua_pushstring(L, nsk__args[i]);
	} else {
		printf("[args] get out of range!\n");
		lua_pushstring(L, "nil");
	}
	return 1;
}

static int args_get_string(lua_State * L)
{
	const char * tag = luaL_checkstring(L, 1);
	const char * def = luaL_checkstring(L, 2);

	for (int i = 2; i < nsk__count - 1; ++i) {
		if (strcmp(tag, nsk__args[i]) == 0 && nsk__args[i + 1][0] != '-') {
			lua_pushstring(L, nsk__args[i + 1]);
			return 1;
		}
	}
	lua_pushstring(L, def);
	return 1;
}

static int args_get_number(lua_State * L)
{
	const char * tag = luaL_checkstring(L, 1);
	double def = (double)luaL_checknumber(L, 2);

	for (int i = 2; i < nsk__count - 1; ++i) {
		if (strcmp(tag, nsk__args[i]) == 0 && nsk__args[i + 1][0] != '-') {
			lua_pushnumber(L, atof(nsk__args[i + 1]));
			return 1;
		}
	}
	lua_pushnumber(L, def);
	return 1;
}

static int args_get_bool(lua_State * L)
{
	const char * tag = luaL_checkstring(L, 1);
	for (int i = 2; i < nsk__count; ++i) {
		if (strcmp(tag, nsk__args[i]) == 0) {
			lua_pushboolean(L, 1);
			return 1;
		}
	}
	lua_pushboolean(L, 0);
	return 1;
}

static int args_count(lua_State * L)
{
	int c = nsk__count - 2;
	if (c < 0) {
		c = 0;
	}
	lua_pushinteger(L, c);
	return 1;
}

// ------------------------------------------------------------------------------

static const luaL_Reg args_namespace[] = {
    {"get", args_get},
    {"count", args_count},
    {"get_bool", args_get_bool},
    {"get_string", args_get_string},
    {"get_number", args_get_number},
    {NULL, NULL}};

LUALIB_API int luaopen_args(lua_State * L)
{
	luaL_register(L, "args", args_namespace);
	return 1;
}
