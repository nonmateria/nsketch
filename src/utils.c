#include "utils.h"

#include <stdio.h>
#include <string.h>

int nsk_get_name_from_path(const char * path, char * buffer, unsigned buffersize)
{
	if (path == NULL) {
		printf("nsk_get_name_from_path error : path is NULL\n");
		return 1;
	}

	if (buffer == NULL) {
		printf("nsk_get_name_from_path error : buffer is NULL\n");
		return 1;
	}

	size_t len = strlen(path);

	size_t last_slash = 0;
	size_t name_end = len - 1;
	if (path[len - 1] == '/') {
		name_end--;
	}

	for (size_t i = 0; i < len - 1; ++i) {
		if (path[i] == '/') {
			last_slash = i + 1;
		}
	}

	for (size_t i = last_slash; i < len; ++i) {
		if (path[i] == '.') {
			name_end = i - 1;
		}
	}

	size_t k = 0;
	for (size_t i = last_slash; i <= name_end; ++i) {
		buffer[k] = path[i];
		k++;
		if (k == buffersize) {
			goto sizeerror;
		}
	}
	if (k < buffersize) {
		buffer[k] = '\0';
	} else {
		goto sizeerror;
	}

	return 0;

sizeerror:
	printf("nsk_get_name_from_path warning: insufficient space in buffer\n");
	return 1;
}

int nsk_path_is_lua_file(const char * path, unsigned len)
{
	if (len < 4) {
		return 0;
	}

	if (path[len - 4] == '.' &&
	    path[len - 3] == 'l' &&
	    path[len - 2] == 'u' &&
	    path[len - 1] == 'a') {
		return 1;
	}

	return 0;
}

int nsk_match_ext(const char * path, const char * ext)
{
	if (path == NULL || ext == NULL) {
		printf("nsk_match_ext: NULL path or ext given!\n");
		return 0;
	}
	size_t ext_len = strlen(ext);
	size_t path_len = strlen(path);
	if (path_len <= ext_len) {
		return 0;
	}

	//printf("trying to match %s (%d) and %s (%d)\n", path, path_len, ext, ext_len );

	size_t k = path_len - ext_len;
	for (unsigned i = 0; i < ext_len; ++i) {
		//printf("matching %c and %c\n", path[k], ext[i]  );
		if (path[k] != ext[i]) {
			return 0;
		}
		k++;
		//printf("..OK\n");
	}

	return 1;
}