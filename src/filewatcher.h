#pragma once

#include <sys/inotify.h>
#define EVENT_MAX_QUEUE 4
#define EVENT_SIZE (sizeof(struct inotify_event))
#define EVENT_BUF_LEN (EVENT_MAX_QUEUE * (EVENT_SIZE + 16))

typedef struct filewatcher_t {
	int ifd;
	int wd;
	char evb[EVENT_BUF_LEN];

} filewatcher_t;

filewatcher_t * filewatcher_create(const char * file);
void filewatcher_destroy(filewatcher_t * watcher);

int filewatcher_change_detected(filewatcher_t * watcher);
