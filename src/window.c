
#include "namespaces.h"

#include "raylib.h"
#include "rlgl.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "utils.h"

#define NSK_WINDOW_NOFADEIN 1
#define NSK_WINDOW_FADEIN 2
#define NSK_WINDOW_LOOP 3
#define NSK_WINDOW_FADEOUT 4
#define NSK_WINDOW_QUIT 0

static int nsk__window_phase = NSK_WINDOW_NOFADEIN;
static float nsk__fadein = 0.0f;
static float nsk__fadeout = 0.0f;
static float nsk__fadepct = 1.0f;
static float nsk__fadeamt = 1.0f;
static int nsk__window_not_created = 1;

// ------------------------------------------------------------------------------
int nsk_should_continue(void)
{
	return nsk__window_phase;
}

unsigned char nsk_prepare_window(float delta)
{
	// delta input is in seconds
	switch (nsk__window_phase) {
	case NSK_WINDOW_FADEIN:
		if (nsk__fadein > 0.0f) {
			nsk__fadepct += delta * nsk__fadein;
			if (nsk__fadepct >= 1.0f) {
				nsk__window_phase = NSK_WINDOW_LOOP;
				nsk__fadepct = 1.0f;
			}
		} else {
			nsk__window_phase = NSK_WINDOW_LOOP;
		}
		break;
	case NSK_WINDOW_FADEOUT:
		if (nsk__fadeout > 0.0f) {
			nsk__fadepct -= delta * nsk__fadeout;
			if (nsk__fadepct <= 0.0f) {
				nsk__window_phase = NSK_WINDOW_QUIT;
				nsk__fadepct = 0.0f;
			}
		} else {
			nsk__window_phase = NSK_WINDOW_QUIT;
		}
		break;
	default:
		break;
	}

	rlClearColor(0, 0, 0, 0); // always clear to transparent
	rlClearScreenBuffers();

	float fade = (1.0f - nsk__fadeamt) + nsk__fadeamt * nsk__fadepct;
	return (unsigned char)(fade * 255.0f);
}

void nsk_window_quit(void)
{
	if (nsk__fadeout == 0.0f) {
		nsk__window_phase = NSK_WINDOW_QUIT;
	} else {
		nsk__window_phase = NSK_WINDOW_FADEOUT;
	}
}

// ------------------------------------------------------------------------------
static int nsk_set_window_size(lua_State * L)
{
	int w = (int)luaL_checknumber(L, 1);
	int h = (int)luaL_checknumber(L, 2);

	if (nsk__window_not_created) {
		//SetConfigFlags(FLAG_MSAA_4X_HINT);
		SetConfigFlags(FLAG_VSYNC_HINT | FLAG_WINDOW_UNDECORATED | FLAG_WINDOW_TRANSPARENT);
#ifdef __ARM_ARCH
		InitWindow(GetScreenWidth(), GetScreenHeight(), "nsk_untitled");
		HideCursor();
#else
		InitWindow(w, h, "|::|");
#endif
		nsk__window_not_created = 0;

		// clears window to avoid dirty first frame
		// ( when window is moved on first frame? )
		BeginDrawing();
		rlClearColor(0, 0, 0, 0); // always clear to transparent
		rlClearScreenBuffers();
		EndDrawing();
	}
#ifndef __ARM_ARCH
	else {
		SetWindowMinSize(w, h);
		SetWindowSize(w, h);
	}
#endif
	nsk_set_default_sizes((unsigned)w, (unsigned)h);

	SetTargetFPS(60); // set default fps value
	return 0;
}

static int nsk_window_fade_in(lua_State * L)
{
	float ms = (float)luaL_checknumber(L, 1);
	if (nsk__window_phase == NSK_WINDOW_NOFADEIN) {
		nsk__fadepct = 0.0f;
		float step = 1000.0f / ms;
		nsk__fadein = step;
		nsk__window_phase = NSK_WINDOW_FADEIN;
	}
	return 0;
}

static int nsk_window_fade_out(lua_State * L)
{
	float ms = (float)luaL_checknumber(L, 1);
	float step = 1000.0f / ms;
	nsk__fadeout = step;
	return 0;
}

static int nsk_window_fade_amount(lua_State * L)
{
	float amt = (float)luaL_checknumber(L, 1);
	if (amt < 0.0f) {
		printf("[window] fade mount should be greater than zero\n");
		amt = 0.0f;
	}
	if (amt > 1.0f) {
		printf("[window] fade mount should be 1.0 or less\n");
		amt = 1.0f;
	}
	nsk__fadeamt = amt;
	return 0;
}

static int nsk_window_fade_get_cursor(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)nsk__fadepct);
	return 1;
}

static int nsk_set_window_title(lua_State * L)
{
	const char * t = luaL_checkstring(L, 1);
	SetWindowTitle(t);
	return 0;
}

static int nsk_move_window(lua_State * L)
{
	int x = (int)luaL_checknumber(L, 1);
	int y = (int)luaL_checknumber(L, 2);
	SetWindowPosition(x, y);
	return 0;
}

static int nsk_set_framerate(lua_State * L)
{
	int f = (int)luaL_checknumber(L, 1);
	SetTargetFPS(f);
	return 0;
}

static int nsk_get_framerate(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)GetFPS());
	return 1;
}

static int nsk_width(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)GetScreenWidth());
	return 1;
}

static int nsk_height(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)GetScreenHeight());
	return 1;
}

static int nsk_save(lua_State * L)
{
	const char * path = luaL_checkstring(L, 1);
	TakeScreenshot(path);
	return 0;
}

static int nsk_window_bind_quit(lua_State * L)
{
	nsk_window_quit();
	return 0;
	(void)L;
}

static int nsk_cursor_hide(lua_State * L)
{
	HideCursor();
	return 0;
	(void)L;
}

static int nsk_cursor_show(lua_State * L)
{
	ShowCursor();
	return 0;
	(void)L;
}

// ------------------------------------------------------------------------------

static const luaL_Reg window_namespace[] = {
    {"title", nsk_set_window_title},
    {"position", nsk_move_window},
    {"size", nsk_set_window_size},
    {"framerate", nsk_set_framerate},
    {"fps", nsk_get_framerate},
    {"width", nsk_width},
    {"height", nsk_height},
    {"save", nsk_save},
    {"quit", nsk_window_bind_quit},
    {"hide_cursor", nsk_cursor_hide},
    {"show_cursor", nsk_cursor_show},
    {"fade_in", nsk_window_fade_in},
    {"fade_out", nsk_window_fade_out},
    {"fade_amount", nsk_window_fade_amount},
    {"fade_cursor", nsk_window_fade_get_cursor},
    {NULL, NULL}};

LUALIB_API int luaopen_window(lua_State * L)
{
	luaL_register(L, "window", window_namespace);
	return 1;
}
