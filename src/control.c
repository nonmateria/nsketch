
#include "namespaces.h"

#include "raylib.h"
#include "rlgl.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <stdio.h>
#include <stdlib.h>

// ------------------------------------------------------------------------------

static int key_pressed(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsKeyPressed(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int key_released(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsKeyReleased(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int key_down(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsKeyDown(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int key_up(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsKeyUp(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

// ------------------------------------------------------------------------------

static const luaL_Reg key_namespace[] = {
    {"pressed", key_pressed},
    {"released", key_released},
    {"down", key_down},
    {"up", key_up},
    {NULL, NULL}};

// ------------------------------------------------------------------------------

static const int nsk__gamepad = 0;

static int pad_last_pressed(lua_State * L)
{
	lua_pushinteger(L, GetGamepadButtonPressed());
	return 1;
}

static int pad_pressed(lua_State * L)
{
	int b = (int)luaL_checkinteger(L, 1);
	if (IsGamepadButtonPressed(nsk__gamepad, b)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int pad_released(lua_State * L)
{
	int b = (int)luaL_checkinteger(L, 1);
	if (IsGamepadButtonReleased(nsk__gamepad, b)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int pad_down(lua_State * L)
{
	int b = (int)luaL_checkinteger(L, 1);
	if (IsGamepadButtonDown(nsk__gamepad, b)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int pad_up(lua_State * L)
{
	int b = (int)luaL_checkinteger(L, 1);
	if (IsGamepadButtonUp(nsk__gamepad, b)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int pad_axis(lua_State * L)
{
	int a = (int)luaL_checkinteger(L, 1);
	lua_pushnumber(L, (lua_Number)GetGamepadAxisMovement(nsk__gamepad, a));
	return 1;
}
// ------------------------------------------------------------------------------

static const luaL_Reg pad_namespace[] = {
    {"last_pressed", pad_last_pressed},
    {"axis", pad_axis},
    {"pressed", pad_pressed},
    {"released", pad_released},
    {"down", pad_down},
    {"up", pad_up},
    {NULL, NULL}};

// ------------------------------------------------------------------------------s
static int mouse_pressed(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsMouseButtonPressed(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int mouse_released(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsMouseButtonReleased(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int mouse_down(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsMouseButtonDown(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int mouse_up(lua_State * L)
{
	int k = (int)luaL_checkinteger(L, 1);
	if (IsMouseButtonUp(k)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int mouse_x(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)GetMouseX());
	return 1;
}

static int mouse_y(lua_State * L)
{
	lua_pushnumber(L, (lua_Number)GetMouseY());
	return 1;
}

// ------------------------------------------------------------------------------

static const luaL_Reg mouse_namespace[] = {
    {"pressed", mouse_pressed},
    {"released", mouse_released},
    {"down", mouse_down},
    {"up", mouse_up},
    {"x", mouse_x},
    {"y", mouse_y},
    {NULL, NULL}};

// ------------------------------------------------------------------------------

LUALIB_API int luaopen_control(lua_State * L)
{
	luaL_register(L, "pad", pad_namespace);

	luaL_register(L, "key", key_namespace);

	lua_pushinteger(L, KEY_APOSTROPHE);
	lua_setfield(L, -2, "apostrophe");

	lua_pushinteger(L, KEY_COMMA);
	lua_setfield(L, -2, "comma");

	lua_pushinteger(L, KEY_MINUS);
	lua_setfield(L, -2, "minus");

	lua_pushinteger(L, KEY_PERIOD);
	lua_setfield(L, -2, "period");

	lua_pushinteger(L, KEY_SLASH);
	lua_setfield(L, -2, "slash");

	lua_pushinteger(L, KEY_ZERO);
	lua_setfield(L, -2, "n0");

	lua_pushinteger(L, KEY_ONE);
	lua_setfield(L, -2, "n1");

	lua_pushinteger(L, KEY_TWO);
	lua_setfield(L, -2, "n2");

	lua_pushinteger(L, KEY_THREE);
	lua_setfield(L, -2, "n3");

	lua_pushinteger(L, KEY_FOUR);
	lua_setfield(L, -2, "n4");

	lua_pushinteger(L, KEY_FIVE);
	lua_setfield(L, -2, "n5");

	lua_pushinteger(L, KEY_SIX);
	lua_setfield(L, -2, "n6");

	lua_pushinteger(L, KEY_SEVEN);
	lua_setfield(L, -2, "n7");

	lua_pushinteger(L, KEY_EIGHT);
	lua_setfield(L, -2, "n8");

	lua_pushinteger(L, KEY_NINE);
	lua_setfield(L, -2, "n9");

	lua_pushinteger(L, KEY_SEMICOLON);
	lua_setfield(L, -2, "semicolon");

	lua_pushinteger(L, KEY_EQUAL);
	lua_setfield(L, -2, "equal");

	lua_pushinteger(L, KEY_A);
	lua_setfield(L, -2, "a");

	lua_pushinteger(L, KEY_B);
	lua_setfield(L, -2, "b");

	lua_pushinteger(L, KEY_C);
	lua_setfield(L, -2, "c");

	lua_pushinteger(L, KEY_D);
	lua_setfield(L, -2, "d");

	lua_pushinteger(L, KEY_E);
	lua_setfield(L, -2, "e");

	lua_pushinteger(L, KEY_F);
	lua_setfield(L, -2, "f");

	lua_pushinteger(L, KEY_G);
	lua_setfield(L, -2, "g");

	lua_pushinteger(L, KEY_H);
	lua_setfield(L, -2, "h");

	lua_pushinteger(L, KEY_I);
	lua_setfield(L, -2, "i");

	lua_pushinteger(L, KEY_J);
	lua_setfield(L, -2, "j");

	lua_pushinteger(L, KEY_K);
	lua_setfield(L, -2, "k");

	lua_pushinteger(L, KEY_L);
	lua_setfield(L, -2, "l");

	lua_pushinteger(L, KEY_M);
	lua_setfield(L, -2, "m");

	lua_pushinteger(L, KEY_N);
	lua_setfield(L, -2, "n");

	lua_pushinteger(L, KEY_O);
	lua_setfield(L, -2, "o");

	lua_pushinteger(L, KEY_P);
	lua_setfield(L, -2, "p");

	lua_pushinteger(L, KEY_Q);
	lua_setfield(L, -2, "q");

	lua_pushinteger(L, KEY_R);
	lua_setfield(L, -2, "r");

	lua_pushinteger(L, KEY_S);
	lua_setfield(L, -2, "s");

	lua_pushinteger(L, KEY_T);
	lua_setfield(L, -2, "t");

	lua_pushinteger(L, KEY_U);
	lua_setfield(L, -2, "u");

	lua_pushinteger(L, KEY_V);
	lua_setfield(L, -2, "v");

	lua_pushinteger(L, KEY_W);
	lua_setfield(L, -2, "w");

	lua_pushinteger(L, KEY_X);
	lua_setfield(L, -2, "x");

	lua_pushinteger(L, KEY_Y);
	lua_setfield(L, -2, "y");

	lua_pushinteger(L, KEY_Z);
	lua_setfield(L, -2, "z");

	lua_pushinteger(L, KEY_SPACE);
	lua_setfield(L, -2, "space");

	lua_pushinteger(L, KEY_ESCAPE);
	lua_setfield(L, -2, "escape");

	lua_pushinteger(L, KEY_ENTER);
	lua_setfield(L, -2, "enter");

	lua_pushinteger(L, KEY_TAB);
	lua_setfield(L, -2, "tab");

	lua_pushinteger(L, KEY_BACKSPACE);
	lua_setfield(L, -2, "backspace");

	lua_pushinteger(L, KEY_INSERT);
	lua_setfield(L, -2, "insert");

	lua_pushinteger(L, KEY_DELETE);
	lua_setfield(L, -2, "delete");

	lua_pushinteger(L, KEY_RIGHT);
	lua_setfield(L, -2, "right_arrow");

	lua_pushinteger(L, KEY_LEFT);
	lua_setfield(L, -2, "left_arrow");

	lua_pushinteger(L, KEY_DOWN);
	lua_setfield(L, -2, "down_arrow");

	lua_pushinteger(L, KEY_UP);
	lua_setfield(L, -2, "up_arrow");

	lua_pushinteger(L, KEY_PAGE_UP);
	lua_setfield(L, -2, "page_up");

	lua_pushinteger(L, KEY_PAGE_DOWN);
	lua_setfield(L, -2, "page_down");

	lua_pushinteger(L, KEY_HOME);
	lua_setfield(L, -2, "home");

	lua_pushinteger(L, KEY_END);
	lua_setfield(L, -2, "end");

	lua_pushinteger(L, KEY_CAPS_LOCK);
	lua_setfield(L, -2, "caps_lock");

	lua_pushinteger(L, KEY_SCROLL_LOCK);
	lua_setfield(L, -2, "scroll_lock");

	lua_pushinteger(L, KEY_NUM_LOCK);
	lua_setfield(L, -2, "num_lock");

	lua_pushinteger(L, KEY_PRINT_SCREEN);
	lua_setfield(L, -2, "print");

	lua_pushinteger(L, KEY_PAUSE);
	lua_setfield(L, -2, "pause");

	lua_pushinteger(L, KEY_F1);
	lua_setfield(L, -2, "f1");

	lua_pushinteger(L, KEY_F2);
	lua_setfield(L, -2, "f2");

	lua_pushinteger(L, KEY_F3);
	lua_setfield(L, -2, "f3");

	lua_pushinteger(L, KEY_F4);
	lua_setfield(L, -2, "f4");

	lua_pushinteger(L, KEY_F5);
	lua_setfield(L, -2, "f5");

	lua_pushinteger(L, KEY_F6);
	lua_setfield(L, -2, "f6");

	lua_pushinteger(L, KEY_F7);
	lua_setfield(L, -2, "f7");

	lua_pushinteger(L, KEY_F8);
	lua_setfield(L, -2, "f8");

	lua_pushinteger(L, KEY_F9);
	lua_setfield(L, -2, "f9");

	lua_pushinteger(L, KEY_F10);
	lua_setfield(L, -2, "f10");

	lua_pushinteger(L, KEY_F11);
	lua_setfield(L, -2, "f11");

	lua_pushinteger(L, KEY_F12);
	lua_setfield(L, -2, "f12");

	lua_pushinteger(L, KEY_LEFT_SHIFT);
	lua_setfield(L, -2, "left_shift");

	lua_pushinteger(L, KEY_LEFT_CONTROL);
	lua_setfield(L, -2, "left_control");

	lua_pushinteger(L, KEY_LEFT_ALT);
	lua_setfield(L, -2, "left_alt");

	lua_pushinteger(L, KEY_LEFT_SUPER);
	lua_setfield(L, -2, "left_super");

	lua_pushinteger(L, KEY_RIGHT_SHIFT);
	lua_setfield(L, -2, "right_shift");

	lua_pushinteger(L, KEY_RIGHT_CONTROL);
	lua_setfield(L, -2, "right_control");

	lua_pushinteger(L, KEY_RIGHT_ALT);
	lua_setfield(L, -2, "right_alt");

	lua_pushinteger(L, KEY_RIGHT_SUPER);
	lua_setfield(L, -2, "right_super");

	lua_pushinteger(L, KEY_KB_MENU);
	lua_setfield(L, -2, "kb_menu");

	lua_pushinteger(L, KEY_LEFT_BRACKET);
	lua_setfield(L, -2, "left_bracket");

	lua_pushinteger(L, KEY_RIGHT_BRACKET);
	lua_setfield(L, -2, "right_bracket");

	lua_pushinteger(L, KEY_BACKSLASH);
	lua_setfield(L, -2, "backslash");

	lua_pushinteger(L, KEY_GRAVE);
	lua_setfield(L, -2, "grave");

	lua_pushinteger(L, KEY_KP_0);
	lua_setfield(L, -2, "kp_0");

	lua_pushinteger(L, KEY_KP_1);
	lua_setfield(L, -2, "kp_1");

	lua_pushinteger(L, KEY_KP_2);
	lua_setfield(L, -2, "kp_2");

	lua_pushinteger(L, KEY_KP_3);
	lua_setfield(L, -2, "kp_3");

	lua_pushinteger(L, KEY_KP_4);
	lua_setfield(L, -2, "kp_4");

	lua_pushinteger(L, KEY_KP_5);
	lua_setfield(L, -2, "kp_5");

	lua_pushinteger(L, KEY_KP_6);
	lua_setfield(L, -2, "kp_6");

	lua_pushinteger(L, KEY_KP_7);
	lua_setfield(L, -2, "kp_7");

	lua_pushinteger(L, KEY_KP_8);
	lua_setfield(L, -2, "kp_8");

	lua_pushinteger(L, KEY_KP_9);
	lua_setfield(L, -2, "kp_9");

	lua_pushinteger(L, KEY_KP_DECIMAL);
	lua_setfield(L, -2, "kp_decimal");

	lua_pushinteger(L, KEY_KP_DIVIDE);
	lua_setfield(L, -2, "kp_divide");

	lua_pushinteger(L, KEY_KP_MULTIPLY);
	lua_setfield(L, -2, "kp_multiply");

	lua_pushinteger(L, KEY_KP_SUBTRACT);
	lua_setfield(L, -2, "kp_subtract");

	lua_pushinteger(L, KEY_KP_ADD);
	lua_setfield(L, -2, "kp_add");

	lua_pushinteger(L, KEY_KP_ENTER);
	lua_setfield(L, -2, "kp_enter");

	lua_pushinteger(L, KEY_KP_EQUAL);
	lua_setfield(L, -2, "kp_equal");

	luaL_register(L, "mouse", mouse_namespace);

	lua_pushinteger(L, MOUSE_LEFT_BUTTON);
	lua_setfield(L, -2, "left");

	lua_pushinteger(L, MOUSE_RIGHT_BUTTON);
	lua_setfield(L, -2, "right");

	lua_pushinteger(L, MOUSE_MIDDLE_BUTTON);
	lua_setfield(L, -2, "middle");

	return 1;
}
