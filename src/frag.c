
#include "namespaces.h"

#include "rlgl.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "filewatcher.h"
#include "utils.h"

#define NSK_MAX_FRAGS 128

static const char * nsk__vdefault = "\n"
                                    "#version 100\n"
                                    "attribute vec3 vertexPosition;\n"
                                    "attribute vec2 vertexTexCoord;\n"
                                    "attribute vec3 vertexNormal;\n"
                                    "attribute vec4 vertexColor;\n"
                                    "uniform mat4 mvp;\n"
                                    "varying vec2 vst;\n"
                                    "varying vec4 fragColor;\n"
                                    "void main()\n"
                                    "{\n"
                                    "    vst = vertexTexCoord;\n"
                                    "    fragColor = vertexColor;\n"
                                    "    gl_Position = mvp*vec4(vertexPosition, 1.0);\n"
                                    "}\n";

typedef struct nsk_frag_t {
	Shader shader;
	filewatcher_t * watcher;
	char * path;
	char * name;
} nsk_frag_t;

static nsk_frag_t frags[NSK_MAX_FRAGS];
static unsigned nsk__frags_size = 0;
static unsigned nsk__fi = 0;

#define NSK_NAMEBUFFER_SIZE 1024
static char nsk__namebuffer[NSK_NAMEBUFFER_SIZE];

// ------------------------------------------------------------------------------

void nsk_frag_init(void)
{
	for (unsigned i = 0; i < NSK_MAX_FRAGS; ++i) {
		frags[i].watcher = NULL;
		frags[i].path = NULL;
		frags[i].name = NULL;
	}
}

void nsk_frag_release(void)
{
	for (unsigned i = 0; i < NSK_MAX_FRAGS; ++i) {
		if (frags[i].watcher != NULL) {
			filewatcher_destroy(frags[i].watcher);
		}
		if (frags[i].path != NULL) {
			free(frags[i].path);
			UnloadShader(frags[i].shader);
		}
		if (frags[i].name != NULL) {
			free(frags[i].name);
		}
	}
}

void nsk__frag_reload(unsigned i, unsigned unload_first)
{
	if (unload_first) {
		if (frags[i].shader.id != rlGetShaderIdDefault()) {
			UnloadShader(frags[i].shader);
		}
	}

	int fd = open(frags[i].path, O_RDONLY);
	size_t fsize = (size_t)lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);

	// TODO: use a single buffer allocate just once,
	// to reduce memory management
	const char prepend[] = "#version 100\n"
	                       "#define u_buffer texture0\n";
	size_t prepend_size = sizeof(prepend) - 1; // don't copy \0

	char * code = malloc(fsize + 1 + prepend_size);

	memmove(code, prepend, prepend_size); // copy prepended code

	read(fd, code + prepend_size, (size_t)fsize); // copy file code
	code[fsize + prepend_size] = '\0';            // finalize

#ifdef __ARM_ARCH
	// working on rpi but will give more warnings
	frags[i].shader = LoadShaderFromMemory(nsk__vdefault, code);
#else
	// taken from raylib LoadShaderFromMemory()
	frags[i].shader = (Shader){0};
	frags[i].shader.locs = (int *)RL_CALLOC(RL_MAX_SHADER_LOCATIONS, sizeof(int));
	frags[i].shader.id = rlLoadShaderCode(nsk__vdefault, code);
#endif

	free(code);
	close(fd);

	printf("[frag] file %s reloaded | name: %s\n", frags[i].path, frags[i].name);
}

void nsk_frag_watch()
{
	for (unsigned i = 0; i < nsk__frags_size; ++i) {
		if (frags[i].watcher != NULL && filewatcher_change_detected(frags[i].watcher)) {
			nsk__frag_reload(i, 1);
		}
	}
}

Shader nsk_get_shader(void)
{
	return frags[nsk__fi].shader;
}

// ------------------------------------------------------------------------------

static int frag_load(lua_State * L)
{
	const char * path = luaL_checkstring(L, 1);
	const char * name = NULL;

	int opt = lua_gettop(L);
	if (opt > 1) {
		name = luaL_checkstring(L, 2);
	} else {
		nsk_get_name_from_path(path, nsk__namebuffer, NSK_NAMEBUFFER_SIZE);
		name = nsk__namebuffer;
	}

	unsigned i = 0;
	for (; i < nsk__frags_size; ++i) {
		if (frags[i].path != NULL && (strcmp(path, frags[i].path) == 0)) {
			// already loaded and watched for changes
			if (strcmp(name, frags[i].name) != 0) {
				size_t len = strlen(name);
				free(frags[i].name);
				frags[i].name = malloc(len + 1);
				memmove(frags[i].name, name, len + 1);
			}
			nsk__fi = i;
			return 0;
		}
	}

	nsk__frags_size++;
	i = nsk__frags_size - 1;

	size_t len = strlen(path);
	frags[i].path = malloc(len + 1);
	memmove(frags[i].path, path, len + 1);

	len = strlen(name);
	frags[i].name = malloc(len + 1);
	memmove(frags[i].name, name, len + 1);

	frags[i].watcher = filewatcher_create(path);

	nsk__frag_reload(i, 0);

	return 0;
}

static int frag_code(lua_State * L)
{
	const char * input_code = luaL_checkstring(L, 1);
	const char * name = luaL_checkstring(L, 2);

	int found = 0;
	for (unsigned i = 0; i < nsk__frags_size; ++i) {
		if (frags[i].name != NULL && strcmp(name, frags[i].name) == 0) {
			nsk__fi = i;
			found = 1;
			if (frags[i].shader.id != rlGetShaderIdDefault()) {
				UnloadShader(frags[i].shader);
			}
		}
	}

	if (!found) {
		nsk__frags_size++;
		nsk__fi = nsk__frags_size - 1;
		// copy name
		size_t len = strlen(name);
		frags[nsk__fi].name = malloc(len + 1);
		memmove(frags[nsk__fi].name, name, len + 1);
	}

	const char prepend[] = "#version 100\n"
	                       "#define u_input_frame texture0\n";
	size_t prepend_size = sizeof(prepend) - 1; // don't copy \0

	size_t input_code_size = strlen(input_code);

	char * code = malloc(input_code_size + prepend_size + 1);

	// copy prepended code
	memmove(code, prepend, prepend_size);
	// copy input code
	memmove(code + prepend_size, input_code, input_code_size);
	// add zero termination
	code[input_code_size + prepend_size] = '\0';

#ifdef __ARM_ARCH
	// working on rpi but will give more warnings
	frags[nsk__fi].shader = LoadShaderFromMemory(nsk__vdefault, code);
#else
	// taken from raylib LoadShaderFromMemory()
	frags[nsk__fi].shader = (Shader){0};
	frags[nsk__fi].shader.locs = (int *)RL_CALLOC(RL_MAX_SHADER_LOCATIONS, sizeof(int));
	frags[nsk__fi].shader.id = rlLoadShaderCode(nsk__vdefault, code);
#endif

	free(code);

	return 0;
}

static void nsk__begin_shader(void)
{
	RenderTexture now = nsk_layer_get_current_fbo();
	RenderTexture other = nsk_layer_get_other_fbo();

	BeginTextureMode(other);

	ClearBackground((Color){0, 0, 0, 0});

	BeginShaderMode(frags[nsk__fi].shader);

	float reso[2];
	reso[0] = (float)now.texture.width;
	reso[1] = (float)now.texture.height;

	int tloc = GetShaderLocation(frags[nsk__fi].shader, "u_time");
	if (tloc != -1) {
		float time = (float)nsk_clock();
		SetShaderValue(frags[nsk__fi].shader, tloc, &time, SHADER_UNIFORM_FLOAT);
	}

	int rloc = GetShaderLocation(frags[nsk__fi].shader, "u_resolution");
	if (rloc != -1) {
		SetShaderValue(frags[nsk__fi].shader, rloc, reso, SHADER_UNIFORM_VEC2);
	}
}

static void nsk__close_shader(void)
{
	RenderTexture now = nsk_layer_get_current_fbo();
	DrawTextureRec(now.texture,
	               (Rectangle){0, 0, (float)now.texture.width, -(float)now.texture.height},
	               (Vector2){0, 0}, WHITE);
	EndShaderMode();
	EndTextureMode();
}

static int frag_select(lua_State * L)
{
	const char * name = luaL_checkstring(L, 1);
	for (unsigned i = 0; i < nsk__frags_size; ++i) {
		if (strcmp(name, frags[i].name) == 0) {
			nsk__fi = i;

			nsk_layer_close();
			nsk__begin_shader();

			return 0;
		}
	}
	printf("[frag] no frag %s found\n", name);
	return 0;
}

static int frag_apply(lua_State * L)
{
	int opt = lua_gettop(L);
	if (opt > 0) {
		int notfound = 1;
		const char * name = luaL_checkstring(L, 1);
		for (unsigned i = 0; i < nsk__frags_size; ++i) {
			if (strcmp(name, frags[i].name) == 0) {
				nsk__fi = i;
				notfound = 0;
			}
		}
		if (notfound) {
			printf("[frag] no frag %s found\n", name);
			return 0;
		}

		nsk_layer_close();
		nsk__begin_shader();
	}

	nsk__close_shader();
	nsk_layer_swap_fbos();
	nsk_layer_open();

	return 0;
}

static int frag_uniform(lua_State * L)
{
	int opt = lua_gettop(L);
	float values[4];
	if (opt < 2) {
		printf("[frag] not enought values for frag.uniform!\n");
		return 0;
	}
	const char * uni_name = luaL_checkstring(L, 1);
	values[0] = (float)luaL_checknumber(L, 2);
	values[1] = (float)luaL_optnumber(L, 3, 0.0f);
	values[2] = (float)luaL_optnumber(L, 4, 0.0f);
	values[3] = (float)luaL_optnumber(L, 5, 0.0f);

	int loc = GetShaderLocation(frags[nsk__fi].shader, uni_name);

	if (loc != -1) {
		switch (opt) {
		case 2:
			SetShaderValue(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_FLOAT);
			break;
		case 3:
			SetShaderValue(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_VEC2);
			break;
		case 4:
			SetShaderValue(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_VEC3);
			break;
		case 5:
			SetShaderValue(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_VEC4);
			break;
		}
	}
	return 0;
}

static int frag_uniform_i(lua_State * L)
{
	int opt = lua_gettop(L);
	int values[4];
	if (opt < 2) {
		printf("[frag] not enought values for frag.uniform!\n");
		return 0;
	}
	const char * uni_name = luaL_checkstring(L, 1);
	values[0] = (int)luaL_checkinteger(L, 2);
	values[1] = (int)luaL_optinteger(L, 3, 0);
	values[2] = (int)luaL_optinteger(L, 4, 0);
	values[3] = (int)luaL_optinteger(L, 5, 0);
	int loc = GetShaderLocation(frags[nsk__fi].shader, uni_name);

	if (loc != -1) {
		switch (opt) {
		case 2:
			SetShaderValue(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_INT);
			break;
		case 3:
			SetShaderValueV(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_IVEC2, 2);
			break;
		case 4:
			SetShaderValueV(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_IVEC3, 3);
			break;
		case 5:
			SetShaderValueV(frags[nsk__fi].shader, loc, values, SHADER_UNIFORM_IVEC4, 4);
			break;
		}
	}
	return 0;
}

// ------------------------------------------------------------------------------

static const luaL_Reg frag_namespace[] = {
    {"load", frag_load},
    {"code", frag_code},
    {"select", frag_select},
    {"apply", frag_apply},
    {"uniform", frag_uniform},
    {"uniform_i", frag_uniform_i},
    {NULL, NULL}};

LUALIB_API int luaopen_frag(lua_State * L)
{
	luaL_register(L, "frag", frag_namespace);
	return 1;
}
