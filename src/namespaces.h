#pragma once

#include <lauxlib.h>
#include <lua.h>
#include <stdlib.h>

#include "raylib.h"

LUALIB_API int luaopen_nsk(lua_State * L);
LUALIB_API int luaopen_window(lua_State * L);
LUALIB_API int luaopen_layer(lua_State * L);
LUALIB_API int luaopen_frag(lua_State * L);
LUALIB_API int luaopen_osc(lua_State * L);
LUALIB_API int luaopen_png(lua_State * L);
LUALIB_API int luaopen_ps(lua_State * L);
LUALIB_API int luaopen_fn(lua_State * L);
LUALIB_API int luaopen_control(lua_State * L);
LUALIB_API int luaopen_args(lua_State * L);
LUALIB_API int luaopen_cam(lua_State * L);
LUALIB_API int luaopen_audio(lua_State * L);

unsigned char nsk_prepare_window(float delta);
int nsk_should_continue(void);
void nsk_window_quit(void);

void nsk_canvas_set_defaults(void);
void nsk_canvas_release(void);

void nsk_layer_init(void);
void nsk_set_default_sizes(unsigned w, unsigned h);
void nsk_layer_reset(void);
void nsk_layer_open(void);
void nsk_layer_close(void);
void nsk_layer_render(unsigned char alpha);
void nsk_layer_release(void);

void nsk_frag_init(void);
void nsk_frag_release(void);
void nsk_frag_watch(void);
Shader nsk_get_shader(void);

void nsk_png_init(void);
void nsk_png_set_defaults(void);
void nsk_png_release(void);

void nsk_ps_set_defaults(void);
void nsk_ps_release(void);

void nsk_lfo_init(void);
float nsk_clock_update(void);

void nsk_osc_init(void);
void nsk_osc_update(void);
void nsk_osc_release(void);

void nsk_cam_update(void);
void nsk_cam_release(void);

void nsk_jack_init(void);
void nsk_jack_close(void);

void nsk_args_init(int argc, char ** argv);

RenderTexture nsk_layer_get_current_fbo(void);
RenderTexture nsk_layer_get_other_fbo(void);
void nsk_layer_swap_fbos(void);

unsigned nsk_dice(unsigned max);
float nsk_range(float min, float max);

double nsk_clock(void);
