
#include "namespaces.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define SOKOL_IMPL
#include "3rdp/sokol_time.h"
#undef SOKOL_IMPL

#define RND_IMPLEMENTATION
#include "3rdp/rnd.h"

#include "3rdp/simplexnoise1234.h"

#include "utils.h"

rnd_pcg_t nsk__pnrg;

static uint64_t nsk__ticks = 0;
static long int nsk__clock_frames = 0;
static double nsk__clock = 0.0;
static double nsk__timewarp = 1.0;
static double nsk__delta = 1.0 / 60.0;

// ------------------------------------------------------------------------------
unsigned linux_get_devrandom(void)
{
	unsigned seed = (unsigned)time(NULL); // fallback

	int random_fd = -1;
	random_fd = open("/dev/urandom", O_RDONLY);

	if (random_fd == -1) {
		fprintf(stderr,
		        "[nsketch] error opening /dev/urandom: "
		        "errno [%d], strerrer [%s], falling back to time(NULL)\n",
		        errno, strerror(errno));
	} else {
		ssize_t ret = 0;
		uint8_t buf[4];
		ret = read(random_fd, buf, 4);
		if (ret != 4) {
			fprintf(stderr,
			        "Only read [%d] bytes, expected 4,"
			        " falling back to time(NULL)\n",
			        (int)ret);
		} else {
			seed = (unsigned)(buf[3] + (buf[2] << 8) + (buf[1] << 16) + (buf[0] << 24));
		}
		close(random_fd);
	}
	return seed;
}

void nsk_lfo_init(void)
{
	unsigned seed = linux_get_devrandom();
	rnd_pcg_seed(&nsk__pnrg, seed);
	stm_setup();
}

unsigned nsk_dice(unsigned max)
{
	return rnd_pcg_next(&nsk__pnrg) % max;
}

float nsk_range(float min, float max)
{
	if (min > max) {
		float temp = min;
		min = max;
		max = temp;
	}
	float uf = rnd_pcg_nextf(&nsk__pnrg);
	float range = max - min;
	return (min + range * uf);
}

// ------------------------------------------------------------------------------

float nsk_clock_update(void)
{
	uint64_t now = stm_now();
	double delta = stm_sec(stm_diff(now, nsk__ticks));
	nsk__delta = delta;
	nsk__clock += delta * nsk__timewarp;
	nsk__ticks = now;
	nsk__clock_frames++;
	return (float)delta;
}

double nsk_clock(void)
{
	return nsk__clock;
}

// ------------------------------------------------------------------------------

static int clock_get(lua_State * L)
{
	lua_pushnumber(L, nsk__clock);
	return 1;
}

static int clock_delta(lua_State * L)
{
	lua_pushnumber(L, nsk__delta);
	return 1;
}

static int clock_warp(lua_State * L)
{
	nsk__timewarp = (float)luaL_checknumber(L, 1);
	return 0;
}

static int clock_set(lua_State * L)
{
	nsk__clock = (float)luaL_checknumber(L, 1);
	return 0;
}

static int clock_frames(lua_State * L)
{
	lua_pushinteger(L, nsk__clock_frames);
	return 1;
}

// ------------------------------------------------------------------------------
static int fn_map(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float i_min = (float)luaL_checknumber(L, 2);
	float i_max = (float)luaL_checknumber(L, 3);
	float o_min = (float)luaL_optnumber(L, 4, 0.0);
	float o_max = (float)luaL_optnumber(L, 5, 1.0);

	float div = i_max - i_min;
	if (div == 0.0f) { // avoid division by 0
		lua_pushnumber(L, 0.0);
		return 1;
	}

	float slope = 1.0f * (o_max - o_min) / div;
	float output = o_min + slope * (x - i_min);

	if (o_min > o_max) {
		float temp = o_min;
		o_min = o_max;
		o_max = temp;
	}

	if (output < o_min) {
		output = o_min;
	}
	if (output > o_max) {
		output = o_max;
	}

	lua_pushnumber(L, output);
	return 1;
}

static int lfo_triangle(lua_State * L)
{
	double ph = (double)luaL_checknumber(L, 1);
	ph = ph - floor(ph);
	if (ph < 0.0f) {
		ph += 1.0f;
	}
	double out = 1.0 - (fabs(ph - 0.5) * 2.0);
	lua_pushnumber(L, out);
	return 1;
}

static int lfo_ramp(lua_State * L)
{
	double ph = (double)luaL_checknumber(L, 1);
	ph = ph - floor(ph);
	if (ph < 0.0f) {
		ph += 1.0f;
	}
	lua_pushnumber(L, ph);
	return 1;
}

static int lfo_saw(lua_State * L)
{
	double ph = (double)luaL_checknumber(L, 1);
	ph = ph - floor(ph);
	if (ph < 0.0f) {
		ph += 1.0f;
	}
	double out = 1.0 - ph;
	lua_pushnumber(L, out);
	return 1;
}

static int lfo_sine(lua_State * L)
{
	double ph = (double)luaL_checknumber(L, 1);
	ph = ph - floor(ph);
	if (ph < 0.0f) {
		ph += 1.0f;
	}
	double out = 1.0 - (cos(ph * F_TWO_PI) * 0.5 + 0.5);
	lua_pushnumber(L, out);
	return 1;
}

static int lfo_square(lua_State * L)
{
	double ph = (double)luaL_checknumber(L, 1);
	ph = ph - floor(ph);
	if (ph < 0.0f) {
		ph += 1.0f;
	}
	double out = (ph > 0.5) ? 1.0 : 0.0;
	lua_pushnumber(L, out);
	return 1;
}

static int lfo_pulse(lua_State * L)
{
	double ph = (double)luaL_checknumber(L, 1);
	double width = (float)luaL_checknumber(L, 2);
	ph = ph - floor(ph);
	if (ph < 0.0f) {
		ph += 1.0f;
	}
	double out = (ph > width) ? 1.0 : 0.0;
	lua_pushnumber(L, out);
	return 1;
}

static int noise_1d(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	lua_pushnumber(L, snoise1(x) * 0.5f + 0.5f);
	return 1;
}

static int noise_2d(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	lua_pushnumber(L, snoise2(x, y) * 0.5f + 0.5f);
	return 1;
}

static int noise_3d(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float z = (float)luaL_checknumber(L, 3);
	lua_pushnumber(L, snoise3(x, y, z) * 0.5f + 0.5f);
	return 1;
}

static int noise_4d(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float z = (float)luaL_checknumber(L, 3);
	float w = (float)luaL_checknumber(L, 4);
	lua_pushnumber(L, snoise4(x, y, z, w) * 0.5f + 0.5f);
	return 1;
}

static int noise_multi(lua_State * L)
{
	int opt = lua_gettop(L);
	switch (opt) {
	case 1: {
		float x = (float)luaL_checknumber(L, 1);
		lua_pushnumber(L, snoise1(x) * 0.5f + 0.5f);
	} break;

	case 2: {
		float x = (float)luaL_checknumber(L, 1);
		float y = (float)luaL_checknumber(L, 2);
		lua_pushnumber(L, snoise2(x, y) * 0.5f + 0.5f);
	} break;

	case 3: {
		float x = (float)luaL_checknumber(L, 1);
		float y = (float)luaL_checknumber(L, 2);
		float z = (float)luaL_checknumber(L, 3);
		lua_pushnumber(L, snoise3(x, y, z) * 0.5f + 0.5f);
	} break;

	case 4: {
		float x = (float)luaL_checknumber(L, 1);
		float y = (float)luaL_checknumber(L, 2);
		float z = (float)luaL_checknumber(L, 3);
		float w = (float)luaL_checknumber(L, 4);
		lua_pushnumber(L, snoise4(x, y, z, w) * 0.5f + 0.5f);
	} break;

	case 0:
	default:
		printf("[fn] fn.noise() must have from 1 to 4 arguments\n");
		break;
	}

	return 1;
}

// ------------------------------------------------------------------------------

static int rand_dice(lua_State * L)
{
	unsigned max = (unsigned)luaL_checkinteger(L, 1);
	unsigned result = rnd_pcg_next(&nsk__pnrg) % max;
	lua_pushnumber(L, (lua_Number)result);
	return 1;
}

static int rand_one(lua_State * L)
{
	lua_Number result = rnd_pcg_nextf(&nsk__pnrg);
	lua_pushnumber(L, result);
	return 1;
}

static int rand_range(lua_State * L)
{
	lua_Number min = luaL_checknumber(L, 1);
	lua_Number max = luaL_checknumber(L, 2);
	if (min > max) {
		lua_Number temp = min;
		min = max;
		max = temp;
	}
	lua_Number uf = rnd_pcg_nextf(&nsk__pnrg);
	lua_Number range = max - min;
	lua_pushnumber(L, min + range * uf);
	return 1;
}

static int rand_chance(lua_State * L)
{
	lua_Number t = luaL_checknumber(L, 1);
	if (t <= 0.0) {
		lua_pushboolean(L, 0);
		return 1;
	}
	if (t >= 1.0) {
		lua_pushboolean(L, 1);
		return 1;
	}

	lua_Number f = rnd_pcg_nextf(&nsk__pnrg);
	if (f < t) {
		lua_pushboolean(L, 1);
		return 1;
	}

	lua_pushboolean(L, 0);
	return 0;
}

// ------------------------------------------------------------------------------
static const luaL_Reg rand_namespace[] = {
    {"dice", rand_dice},
    {"one", rand_one},
    {"om", rand_one},
    {"range", rand_range},
    {"chance", rand_chance},
    {NULL, NULL}};

static const luaL_Reg fn_namespace[] = {
    {"triangle", lfo_triangle},
    {"ramp", lfo_ramp},
    {"saw", lfo_saw},
    {"sine", lfo_sine},
    {"square", lfo_square},
    {"pulse", lfo_pulse},
    {"noise1", noise_1d},
    {"noise2", noise_2d},
    {"noise3", noise_3d},
    {"noise4", noise_4d},
    {"noise", noise_multi},
    {"map", fn_map},
    {NULL, NULL}};

static const luaL_Reg clock_namespace[] = {
    {"get", clock_get},
    {"delta", clock_delta},
    {"set", clock_set},
    {"warp", clock_warp},
    {"frame", clock_frames},
    {NULL, NULL}};

LUALIB_API int luaopen_fn(lua_State * L)
{
	luaL_register(L, "clock", clock_namespace);
	luaL_register(L, "rand", rand_namespace);

	luaL_register(L, "fn", fn_namespace);
	lua_pushnumber(L, F_PI);
	lua_setfield(L, -2, "pi");
	lua_pushnumber(L, F_TWO_PI);
	lua_setfield(L, -2, "two_pi");

	return 1;
}
