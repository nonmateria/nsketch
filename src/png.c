
#include "namespaces.h"

#include "raylib.h"
#include "rlgl.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "utils.h"

#include "3rdp/tinydir.h"

typedef struct nsk_folder_t {
	unsigned index;
	unsigned size;
	Texture * images;
	char * path;
	char * name;
} nsk_folder_t;

#define NSK_IT_IS_A_FILE -42

#define NSK_MAX_FOLDERS 128

static nsk_folder_t folders[NSK_MAX_FOLDERS];
static unsigned nsk__folders_size = 0;
static unsigned nsk__fi = 0;

static Color nsk__color;
static int nsk__align = 0;

#define NSK_NAMEBUFFER_SIZE 1024
static char nsk__namebuffer[NSK_NAMEBUFFER_SIZE];

const char * nsk__png_extensions[] = {
    ".png",
    ".PNG",
    ".jpg",
    ".jpeg",
    ".JPG",
    ".JPEG",
    ".gif",
    ".GIF",
    ".tga",
    ".TGA",
    ".bmp",
    ".BMP",
    ".psd",
    ".PSD",
    ".hdr",
    ".HDR",
    ".pic",
    ".PIC",
    ".pnm",
    ".PNM"};
unsigned nsk__ext_num = sizeof(nsk__png_extensions) / sizeof(char *);

// ------------------------------------------------------------------------------

void nsk_png_init()
{
	for (unsigned i = 0; i < NSK_MAX_FOLDERS; ++i) {
		folders[i].path = NULL;
		folders[i].name = NULL;
		folders[i].size = 0;
		folders[i].index = 0;
		folders[i].images = NULL;
	}
}

void nsk_png_set_defaults()
{
	nsk__color.r = 255;
	nsk__color.g = 255;
	nsk__color.b = 255;
	nsk__color.a = 255;
	nsk__align = 0;
}

void nsk_png_release()
{
	for (unsigned i = 0; i < NSK_MAX_FOLDERS; ++i) {
		if (folders[i].path != NULL) {
			free(folders[i].path);
		}
		if (folders[i].name != NULL) {
			free(folders[i].name);
		}
		if (folders[i].images != NULL) {
			for (unsigned k = 0; k < folders[i].size; ++k) {
				UnloadTexture(folders[i].images[k]);
			}
			free(folders[i].images);
		}
	}
}

static void nsk__png_reload_folder(unsigned i, int count)
{
	if (count == NSK_IT_IS_A_FILE) { // never on reload
		folders[i].images = malloc(sizeof(Texture));
		folders[i].size = 1;
		folders[i].images[0] = LoadTexture(folders[i].path);
		folders[i].index = 0;
	} else {

		if (folders[i].size != 0) {
			for (unsigned k = 0; k < folders[i].size; ++k) {
				UnloadTexture(folders[i].images[k]);
			}
			free(folders[i].images);
		}

		folders[i].size = (unsigned)count;
		folders[i].index = 0;
		if (folders[i].size != 0) {
			folders[i].images = malloc(sizeof(Texture) * (size_t)count);
			unsigned k = 0;

			tinydir_dir dir;
			tinydir_open_sorted(&dir, folders[i].path);

			for (size_t f = 0; f < dir.n_files; f++) {
				tinydir_file file;
				tinydir_readfile_n(&dir, &file, f);

				for (unsigned e = 0; e < nsk__ext_num; ++e) {
					if (nsk_match_ext(file.path, nsk__png_extensions[e])) {
						folders[i].images[k] = LoadTexture(file.path);
						k++;
					}
				}
			}
			tinydir_close(&dir);
			printf("[png] folder %s loaded | items: %d | name: %s\n",
			       folders[i].path, folders[i].size, folders[i].name);
		}
	}
}

static int nsk__png_count_files(const char * path)
{
	int count = 0;

	tinydir_dir dir;
	int rc = tinydir_open_sorted(&dir, path);

	if (rc == 0) {
		for (size_t i = 0; i < dir.n_files; i++) {
			tinydir_file file;
			tinydir_readfile_n(&dir, &file, i);

			for (unsigned e = 0; e < nsk__ext_num; ++e) {
				if (nsk_match_ext(file.path, nsk__png_extensions[e])) {
					count++;
				}
			}
		}
		tinydir_close(&dir);
	} else {
		for (unsigned e = 0; e < nsk__ext_num; ++e) {
			if (nsk_match_ext(path, nsk__png_extensions[e])) {
				count = NSK_IT_IS_A_FILE;
			}
		}
	}

	return count;
}

static void nsk__png_load_folder(unsigned i)
{
	int c = nsk__png_count_files(folders[i].path);
	nsk__png_reload_folder(i, c);
}

// ------------------------------------------------------------------------------

static int png_color(lua_State * L)
{
	unsigned r = (unsigned)luaL_checkinteger(L, 1);
	unsigned g = (unsigned)luaL_checkinteger(L, 2);
	unsigned b = (unsigned)luaL_checkinteger(L, 3);
	unsigned a = (unsigned)luaL_checkinteger(L, 4);

	unsigned char cr = (unsigned char)(r & 0xff);
	unsigned char cg = (unsigned char)(g & 0xff);
	unsigned char cb = (unsigned char)(b & 0xff);
	unsigned char ca = (unsigned char)(a & 0xff);

	nsk__color.r = cr;
	nsk__color.g = cg;
	nsk__color.b = cb;
	nsk__color.a = ca;

	return 0;
}

static int png_load(lua_State * L)
{
	const char * path = luaL_checkstring(L, 1);
	const char * name = NULL;

	int opt = lua_gettop(L);
	if (opt > 1) {
		name = luaL_checkstring(L, 2);
	} else {
		nsk_get_name_from_path(path, nsk__namebuffer, NSK_NAMEBUFFER_SIZE);
		name = nsk__namebuffer;
	}

	unsigned i = 0;
	for (; i < nsk__folders_size; ++i) {
		if (folders[i].path != NULL && (strcmp(path, folders[i].path) == 0)) {
			// already loaded and watched for changes
			if (strcmp(name, folders[i].name) != 0) {
				size_t len = strlen(name);
				free(folders[i].name);
				folders[i].name = malloc(len + 1);
				memmove(folders[i].name, name, len + 1);
			}
			int count = nsk__png_count_files(path);
			if (!(count == NSK_IT_IS_A_FILE) && count != (int)folders[i].size) {
				nsk__png_reload_folder(i, count);
			}
			nsk__fi = i;
			return 0;
		}
	}

	nsk__folders_size++;
	i = nsk__folders_size - 1;
	nsk__fi = i;

	size_t len = strlen(path);
	folders[i].path = malloc(len + 1);
	memmove(folders[i].path, path, len + 1);

	len = strlen(name);
	folders[i].name = malloc(len + 1);
	memmove(folders[i].name, name, len + 1);

	nsk__png_load_folder(i);

	return 0;
}

static int png_draw(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);
	float scale = (float)luaL_optnumber(L, 3, 1.0);
	float rot = (float)luaL_optnumber(L, 4, 0);
	float flip = 1.0f;
	if (scale < 0.0f) {
		flip = -1.0f;
		scale = -scale;
	}

	Texture * images = folders[nsk__fi].images;
	if (images != NULL) {
		unsigned i = folders[nsk__fi].index;
		float w = (float)images[i].width;
		float h = (float)images[i].height;
		float ws = w * scale;
		float hs = h * scale;

		switch (nsk__align) {
		case 1:
			x -= w * 0.5f * scale;
			y -= h * 0.5f * scale;
			break;
		case 2:
			y -= h * scale;
			break;
		default:
			break;
		}

		DrawTexturePro(images[i],
		               (Rectangle){0, 0, w * flip, h},
		               (Rectangle){x, y, ws, hs}, (Vector2){0, 0},
		               rot, nsk__color);
	}

	return 0;
}

static int png_draw_subsection(lua_State * L)
{
	float x = (float)luaL_checknumber(L, 1);
	float y = (float)luaL_checknumber(L, 2);

	float rx = (float)luaL_checknumber(L, 3);
	float ry = (float)luaL_checknumber(L, 4);
	float rw = (float)luaL_checknumber(L, 5);
	float rh = (float)luaL_checknumber(L, 6);

	if (lua_toboolean(L, 7)) { // flip, false if not present
		rw = -rw;
	}

	Texture * images = folders[nsk__fi].images;
	if (images != NULL) {
		unsigned i = folders[nsk__fi].index;
		DrawTextureRec(images[i],
		               (Rectangle){rx, ry, rw, rh},
		               (Vector2){x, y}, nsk__color);
	}

	return 0;
}

static int png_size(lua_State * L)
{
	Texture * images = folders[nsk__fi].images;
	if (images != NULL) {
		lua_pushinteger(L, folders[nsk__fi].size);
		return 1;
	}
	lua_pushinteger(L, 0);
	return 1;
}

static int png_width(lua_State * L)
{
	Texture * images = folders[nsk__fi].images;
	if (images != NULL) {
		unsigned i = folders[nsk__fi].index;
		int w = images[i].width;
		lua_pushinteger(L, w);
		return 1;
	}
	lua_pushinteger(L, 0);
	return 1;
}

static int png_height(lua_State * L)
{
	Texture * images = folders[nsk__fi].images;
	if (images != NULL) {
		unsigned i = folders[nsk__fi].index;
		int h = images[i].height;
		lua_pushinteger(L, h);
		return 1;
	}
	lua_pushinteger(L, 0);
	return 1;
}

static int png_select(lua_State * L)
{
	const char * name = luaL_checkstring(L, 1);
	for (unsigned i = 0; i < nsk__folders_size; ++i) {
		if (strcmp(name, folders[i].name) == 0) {
			nsk__fi = i;
			return 0;
		}
	}
	printf("[png] no png %s found\n", name);
	return 0;
}

static int png_next(lua_State * L)
{
	unsigned i = folders[nsk__fi].index;
	unsigned max = folders[nsk__fi].size;
	i++;
	if (i >= max) {
		i = 0;
	}
	folders[nsk__fi].index = i;
	lua_pushinteger(L, i);
	return 1;
}

static int png_prev(lua_State * L)
{
	int i = (int)folders[nsk__fi].index;
	int max = (int)folders[nsk__fi].size;
	i--;
	if (i < 0) {
		i = max - 1;
	}
	folders[nsk__fi].index = (unsigned)i;
	lua_pushinteger(L, i);
	return 1;
}

static int png_shuffle(lua_State * L)
{
	unsigned i = folders[nsk__fi].index;
	unsigned max = folders[nsk__fi].size;
	unsigned n = i;

	switch (max) {
	case 0:
	case 1:
		i = 0;
		break;
	case 2:
		i = !i;
		break;
	default:
		while (i == n) {
			n = nsk_dice(max);
		}
		i = n;
		break;
	}
	folders[nsk__fi].index = i;
	lua_pushinteger(L, i);
	return 1;
}

static int png_jump(lua_State * L)
{
	int ji = (int)luaL_checkinteger(L, 1);
	if (ji < 1) {
		printf("[png] jump offset must be greater than 0\n");
		return 0;
	}
	unsigned j = (unsigned)ji;
	unsigned i = folders[nsk__fi].index;
	unsigned max = folders[nsk__fi].size;

	if (max <= 1) {
		folders[nsk__fi].index = 0;
	} else {
		i += j;
		i = i % max;
		folders[nsk__fi].index = i;
	}
	lua_pushinteger(L, i);
	return 1;
}

static int png_frame(lua_State * L)
{
	int si = (int)luaL_checknumber(L, 1);
	if (si < 0) {
		printf("[png] frame index for %s can't be negative\n",
		       folders[nsk__fi].name);
		return 0;
	}

	unsigned i = (unsigned)si;
	unsigned max = folders[nsk__fi].size;
	if (i >= max) {
		printf("[png] frame index greater than \"%s\" size\n",
		       folders[nsk__fi].name);
		return 0;
	}
	folders[nsk__fi].index = i;
	lua_pushinteger(L, i);
	return 1;
}

static int png_is_last(lua_State * L)
{
	if (folders[nsk__fi].index == (folders[nsk__fi].size - 1)) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int png_is_first(lua_State * L)
{
	if (folders[nsk__fi].index == 0) {
		lua_pushboolean(L, 1);
	} else {
		lua_pushboolean(L, 0);
	}
	return 1;
}

static int nsk_align_corner(lua_State * L)
{
	nsk__align = 0;
	return 0;
	(void)L;
}

static int nsk_align_center(lua_State * L)
{
	nsk__align = 1;
	return 0;
	(void)L;
}

static int nsk_align_bottom(lua_State * L)
{
	nsk__align = 2;
	return 0;
	(void)L;
}

static int png_uniform(lua_State * L)
{
	int opt = lua_gettop(L);
	if (opt < 2) {
		printf("[png] not enought values for png.uniform!\n");
		return 0;
	}
	const char * uni_name = luaL_checkstring(L, 1);
	const char * uni_source = luaL_checkstring(L, 2);
	int index = (int)luaL_optinteger(L, 3, 0);

	Shader shader = nsk_get_shader();

	int loc = GetShaderLocation(shader, uni_name);

	if (loc != -1) {
		for (unsigned i = 0; i < nsk__folders_size; ++i) {
			if (strcmp(uni_source, folders[i].name) == 0) {
				// found
				Texture * images = folders[i].images;
				if (images != NULL) {
					Texture tex = images[index];
					SetShaderValueTexture(shader, loc, tex);
					return 0;
				} else {
					printf("[png] png.uniform() no images in folder %s\n",
					       uni_source);
					return 0;
				}
			}
		}
		printf("[png] png.uniform() no png %s found\n", uni_source);
	}
	return 0;
}

// ------------------------------------------------------------------------------

static const luaL_Reg png_namespace[] = {
    {"load", png_load},
    {"select", png_select},
    {"next", png_next},
    {"prev", png_prev},
    {"shuffle", png_shuffle},
    {"jump", png_jump},
    {"frame", png_frame},
    {"is_last", png_is_last},
    {"is_first", png_is_first},
    {"draw", png_draw},
    {"sub", png_draw_subsection},
    {"align_corner", nsk_align_corner},
    {"align_center", nsk_align_center},
    {"align_bottom", nsk_align_bottom},
    {"size", png_size},
    {"width", png_width},
    {"height", png_height},
    {"color", png_color},
    {"uniform", png_uniform},
    {NULL, NULL}};

LUALIB_API int luaopen_png(lua_State * L)
{
	luaL_register(L, "png", png_namespace);
	return 1;
}
