#include "namespaces.h"

#define LUA_LIB

#include "luajit.h"
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <lo/lo.h>

lo_server nsk__osc_receiver = NULL;
lo_address nsk__osc_destination = NULL;

#define NSK_MAX_OSC_ARGS 16
#define NSK_OSC_RINGSIZE 256

typedef struct nsk_osc_arg {
	unsigned char type;
	size_t s_size;
	float f;
	int i;
	char * s;
} nsk_osc_arg;

typedef struct nsk_osc_message {
	char * address;
	size_t a_size;
	int argc;
	nsk_osc_arg args[NSK_MAX_OSC_ARGS];
} nsk_osc_message;

nsk_osc_message nsk__ringbuffer[NSK_OSC_RINGSIZE];
char nsk__osc_port[16];

static unsigned nsk__write = 0;
static unsigned nsk__read = 0;

static int osc_handler(const char * path, const char * types, lo_arg ** argv,
                       int argc, void * data, void * user_data)
{
	if (argc <= NSK_MAX_OSC_ARGS) {
		nsk__write++;
		if (nsk__write == NSK_OSC_RINGSIZE) {
			nsk__write = 0;
		}

		size_t psize = strlen(path) + 1;
		if (psize > nsk__ringbuffer[nsk__write].a_size) {
			if (nsk__ringbuffer[nsk__write].address != NULL) {
				free(nsk__ringbuffer[nsk__write].address);
			}
			nsk__ringbuffer[nsk__write].address = malloc(psize);
		}
		memmove(nsk__ringbuffer[nsk__write].address, path, psize);

		nsk__ringbuffer[nsk__write].argc = argc;

		for (int i = 0; i < argc; ++i) {
			switch (types[i]) {
			case 'i':
				nsk__ringbuffer[nsk__write].args[i].type = 'i';
				nsk__ringbuffer[nsk__write].args[i].i = argv[i]->i;
				break;
			case 'f':
				nsk__ringbuffer[nsk__write].args[i].type = 'f';
				nsk__ringbuffer[nsk__write].args[i].f = argv[i]->f;
				break;
			case 's': {
				size_t sarg_size = strlen(&argv[i]->s) + 1;
				if (sarg_size > nsk__ringbuffer[nsk__write].args[i].s_size) {
					if (nsk__ringbuffer[nsk__write].args[i].s != NULL) {
						free(nsk__ringbuffer[nsk__write].args[i].s);
					}
					nsk__ringbuffer[nsk__write].args[i].s = malloc(sarg_size);
				}
				memmove(nsk__ringbuffer[nsk__write].args[i].s,
				        &argv[i]->s, sarg_size);
				break;
			}
			}
		}
	} else {
		printf("[osc] for now no more than %d OSC arguments are supported,"
		       " message to %s ignored",
		       NSK_MAX_OSC_ARGS, path);
	}

	return 0;

	(void)data;
	(void)user_data;
}

static void osc_error(int num, const char * msg, const char * path)
{
	printf("[osc] liblo server error %d in path %s: %s\n", num, path, msg);
	fflush(stdout);
}

void nsk_osc_init()
{
	for (unsigned i = 0; i < NSK_OSC_RINGSIZE; ++i) {
		nsk__ringbuffer[i].address = NULL;
		nsk__ringbuffer[i].a_size = 0;
		nsk__ringbuffer[i].argc = 0;
		for (unsigned k = 0; k < NSK_MAX_OSC_ARGS; ++k) {
			nsk__ringbuffer[i].args[k].s_size = 0;
			nsk__ringbuffer[i].args[k].s = NULL;
		}
	}
}

void nsk_osc_update()
{
	if (nsk__osc_receiver) { // not NULL
		lo_server_recv_noblock(nsk__osc_receiver, 0);
	}
}

void nsk_osc_release()
{

	for (unsigned i = 0; i < NSK_OSC_RINGSIZE; ++i) {
		if (nsk__ringbuffer[i].address != NULL) {
			free(nsk__ringbuffer[i].address);
		}
		for (unsigned k = 0; k < NSK_MAX_OSC_ARGS; ++k) {
			if (nsk__ringbuffer[i].args[k].s != NULL) {
				free(nsk__ringbuffer[i].args[k].s);
			}
		}
	}

	if (nsk__osc_receiver != NULL) {
		lo_server_free(nsk__osc_receiver);
	}
	if (nsk__osc_destination != NULL) {
		lo_address_free(nsk__osc_destination);
	}
}

// ------------------------------------------------------------------------------

static int osc_setup(lua_State * L)
{
	const char * port = luaL_checkstring(L, 1);

	size_t psize = strlen(port) + 1;
	if (psize < 16 && strcmp(port, nsk__osc_port) != 0) {
		if (nsk__osc_receiver != NULL) {
			lo_server_free(nsk__osc_receiver);
		}
		nsk__osc_receiver = lo_server_new(port, osc_error);
		lo_server_add_method(nsk__osc_receiver, NULL, NULL, osc_handler, NULL);
		memmove(nsk__osc_port, port, psize);
	}

	return 0;
}

static int osc_set_destination(lua_State * L)
{
	const char * ip = luaL_checkstring(L, 1);
	const char * port = luaL_checkstring(L, 2);

	if (nsk__osc_destination != NULL) {
		lo_address_free(nsk__osc_destination);
	}

	nsk__osc_destination = lo_address_new(ip, port);

	return 0;
}

static int osc_send(lua_State * L)
{
	const char * address = luaL_checkstring(L, 1);
	const char * types = luaL_checkstring(L, 2);

	int max = (int)strlen(types);
	int argc = lua_gettop(L);
	if (argc < max + 2) {
		printf("[osc] not enough arguments for sending types: %s\n", types);
		return 0;
	}

	lo_message msg = lo_message_new();

	for (int i = 0; i < max; ++i) {
		switch (types[i]) {
		case 'i':
			lo_message_add_int32(msg, (int)luaL_checkinteger(L, i + 3));
			break;
		case 'f':
			lo_message_add_float(msg, (float)luaL_checknumber(L, i + 3));
			break;
		case 's':
			lo_message_add_string(msg, luaL_checkstring(L, i + 3));
			break;
		}
	}

	lo_send_message(nsk__osc_destination, address, msg);

	lo_message_free(msg);

	return 0;
}

static int osc_has_message(lua_State * L)
{
	if (nsk__read != nsk__write) {
		lua_pushboolean(L, 1);
		return 1;
	}
	lua_pushboolean(L, 0);
	return 1;
}

static int osc_next(lua_State * L)
{
	if (nsk__read != nsk__write) {
		nsk__read++;
		if (nsk__read == NSK_OSC_RINGSIZE) {
			nsk__read = 0;
		}
		lua_pushboolean(L, 1);
		return 1;
	}
	lua_pushboolean(L, 0);
	return 1;
}

static int osc_address(lua_State * L)
{
	lua_pushstring(L, nsk__ringbuffer[nsk__read].address);
	return 1;
}

static int osc_size(lua_State * L)
{
	lua_pushinteger(L, nsk__ringbuffer[nsk__read].argc);
	return 1;
}

static int osc_is_number(lua_State * L)
{
	int i = (int)luaL_checkinteger(L, 1);
	if (i < nsk__ringbuffer[nsk__read].argc) {
		if (nsk__ringbuffer[nsk__read].args[i].type == 'f' ||
		    nsk__ringbuffer[nsk__read].args[i].type == 'i') {
			lua_pushboolean(L, 1);
			return 1;
		} else {
			lua_pushboolean(L, 0);
			return 1;
		}
	} else {
		printf("[osc] argument index %d outside message size\n", i);
	}

	lua_pushboolean(L, 0);
	return 1;
}

static int osc_is_int32(lua_State * L)
{
	int i = (int)luaL_checkinteger(L, 1);
	if (i < nsk__ringbuffer[nsk__read].argc) {
		if (nsk__ringbuffer[nsk__read].args[i].type == 'i') {
			lua_pushboolean(L, 1);
		} else {
			lua_pushboolean(L, 0);
		}
	} else {
		printf("[osc] argument index %d outside message size\n", i);
	}

	lua_pushinteger(L, nsk__ringbuffer[i].argc);
	return 1;
}

static int osc_is_float(lua_State * L)
{
	int i = (int)luaL_checkinteger(L, 1);
	if (i < nsk__ringbuffer[nsk__read].argc) {
		if (nsk__ringbuffer[nsk__read].args[i].type == 'f') {
			lua_pushboolean(L, 1);
			return 1;
		} else {
			lua_pushboolean(L, 0);
			return 1;
		}
	} else {
		printf("[osc] argument index %d outside message size\n", i);
	}

	lua_pushboolean(L, 0);
	return 1;
}

static int osc_is_string(lua_State * L)
{
	int i = (int)luaL_checkinteger(L, 1);
	if (i < nsk__ringbuffer[nsk__read].argc) {
		if (nsk__ringbuffer[nsk__read].args[i].type == 's') {
			lua_pushboolean(L, 1);
			return 1;
		} else {
			lua_pushboolean(L, 0);
			return 1;
		}
	} else {
		printf("[osc] argument index %d outside message size\n", i);
	}

	lua_pushboolean(L, 0);
	return 1;
}

static int osc_number(lua_State * L)
{
	int i = (int)luaL_checkinteger(L, 1);
	if (i < nsk__ringbuffer[nsk__read].argc) {
		if (nsk__ringbuffer[nsk__read].args[i].type == 'i') {
			lua_pushnumber(L, (lua_Number)nsk__ringbuffer[nsk__read].args[i].i);
			return 1;
		} else if (nsk__ringbuffer[nsk__read].args[i].type == 'f') {
			lua_pushnumber(L, (lua_Number)nsk__ringbuffer[nsk__read].args[i].f);
			return 1;
		} else {
			printf("[osc] the argument %d is not a number\n", i);
		}
	} else {
		printf("[osc] argument index %d outside message size\n", i);
	}

	lua_pushnumber(L, 0.0);
	return 1;
}

static int osc_string(lua_State * L)
{
	int i = (int)luaL_checkinteger(L, 1);
	if (i < nsk__ringbuffer[nsk__read].argc) {
		if (nsk__ringbuffer[nsk__read].args[i].type == 's') {
			lua_pushstring(L, nsk__ringbuffer[nsk__read].args[i].s);
			return 1;
		} else {
			printf("[osc] the argument %d is not a string\n", i);
		}
	} else {
		printf("[osc] argument index %d outside message size\n", i);
	}

	lua_pushstring(L, "nil");
	return 1;
}

// ------------------------------------------------------------------------------

static const luaL_Reg osc_namespace[] = {
    {"receiver", osc_setup},
    {"sender", osc_set_destination},
    {"has_message", osc_has_message},
    {"next", osc_next},
    {"is_number", osc_is_number},
    {"is_int32", osc_is_int32},
    {"is_float", osc_is_float},
    {"is_string", osc_is_string},
    {"address", osc_address},
    {"size", osc_size},
    {"get_number", osc_number},
    {"get_string", osc_string},
    {"send", osc_send},
    {NULL, NULL}};

LUALIB_API int luaopen_osc(lua_State * L)
{
	luaL_register(L, "osc", osc_namespace);
	return 1;
}
