# nsketch 

luajit interpreter for livecoding graphics by using shaders, images and minimal 2d graphics. For example, it can do things like [this](http://nonmateria.com/posts/2021_04_24__naresh_ran_-_kutna_hora.html) and [this](http://nonmateria.com/posts/2021_05_09__stelvio_and_raskol-mekov_-_maremoto.html). It owns a lot to [raylib](https://github.com/raysan5/raylib), that is used for windowing, 2d primitives, text, image loading and shaders. Code is hotreloaded on save by using [inotify](https://man7.org/linux/man-pages/man7/inotify.7.html) and webcam is managed with linux `v4l2` drivers, so for now this tool is linux-only.

namespace list:

* `args` : access cli args listed after the script path
* `audio` : connects to JACK to get audio envelope 
* `cam` : webcam input 
* `clock` : gets and warps time 
* `fn` : various functions to shape inputs ( and noise up to 4d )
* `frag` : manage shaders
* `gfx` : 2d primitives and other various functions
* `key` : keyboard input 
* `layer` : manage different fbo object used as graphical "layers"
* `mouse` : mouse input
* `pad` : gamepad input
* `png` : load images (not just png)
* `rand` : various random functions 
* `window`: manage window size, position, background and title 

the code is functional but still a work in progress; for now this is something made by myself for myself. 

you can see some examples in the [scrapbook](https://codeberg.org/nonmateria/scrapbook/) (only the .lua files) and [scriptsuite](https://codeberg.org/nonmateria/scriptsuite).

## building 
this is a memo for myself ( on debian 10 ):

```
sudo apt-get install libv4l-0 libv4l-dev liblo7 liblo-dev jackd1 libjack-dev 
git clone https://codeberg.org/nonmateria/nsketch.git
cd nsketch 

mkdir libs
cd libs 

git clone https://github.com/raysan5/raylib
cd raylib/src
sudo apt install libasound2-dev mesa-common-dev libx11-dev libxrandr-dev libxi-dev xorg-dev libgl1-mesa-dev libglu1-mesa-dev
make 
sudo make install 

cd ..
git clone https://luajit.org/git/luajit-2.0.git
cd luajit-2.0
git checkout v2.1
make

cd ..
make 
```

## many thanks to 
* [raylib](https://github.com/raysan5/raylib)
* [luaJIT](https://luajit.org/)
* [rnd.h](https://github.com/mattiasgustavsson/libs)
* [perlin noise](https://github.com/stegu/perlin-noise)
* [sokol](https://github.com/floooh/sokol) time header lib
* [tinydir](https://github.com/cxong/tinydir)

## license 
Nicola Pisanti MIT License 2020-2021
