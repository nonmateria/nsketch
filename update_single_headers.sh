#!/bin/sh

directory=${PWD##*/}

if [ "$directory" = "nsketch" ]
then

    echo "nsketch single header libraries update"

	git clone https://github.com/mattiasgustavsson/libs.git rnd
	git clone https://github.com/stegu/perlin-noise.git
	git clone https://github.com/floooh/sokol.git
    git clone https://github.com/cxong/tinydir.git
    
    echo "moving files..."
    mv rnd/rnd.h src/3rdp/rnd.h 
    mv perlin-noise/src/simplexnoise1234.h src/3rdp/simplexnoise1234.h
    mv perlin-noise/src/simplexnoise1234.c src/3rdp/simplexnoise1234.c
    mv sokol/sokol_time.h src/3rdp/sokol_time.h
    mv tinydir/tinydir.h src/3rdp/tinydir.h

    echo "deleting repos..."
    rm -rf rnd/
    rm -rf perlin-noise/
    rm -rf sokol/
    rm -rf tinydir/

    echo "...done"

else
    echo "use this script from the nsketch directory"
fi


exit
